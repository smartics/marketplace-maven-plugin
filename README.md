# Marketplace Maven Plugin

## Overview

This [plugin](https://maven.apache.org/plugins/index.html) for
[Maven](https://maven.apache.org/) supports use cases based on the
[Atlassian Marketplace](https://marketplace.atlassian.com).

We use it for our [projectdoc Toolbox](https://www.smartics.eu/confluence/x/GQFk)
and other apps (or add-on) for [Confluence](https://www.atlassian.com/software/confluence)
we have listed on the Atlassian Marketplace.

We decided to use an approach with Maven integration since our automation
processes are mostly Maven-based. Maven makes bundling with third-party
libraries easy and the configuration options are well-known so that others may
also easily integrate the plugin into their automation processes.

The work is based on [atlassian-marketplace-to-mailchimp-lambda](https://github.com/pawelniewie/atlassian-marketplace-to-mailchimp-lambda)
by Pawel Niewiadomski available on GitHub. The tool is based on a
[Pull Request](https://bitbucket.org/atlassian-marketplace/marketplace-client-java/pull-requests/51/added-support-for-vendor-reporting/diff)
that has not been accepted by Atlassian (apparently due to priority reasons).
We would like to use the Java Client provided by Atlassian for our plugin for
Maven, which still lacks  services for accessing license information. Therefore
we workaround the fact that the Pull Request has not been accepted.


## Current State
We have just started with this project. Currently we get the part that accesses
the Marketplace API to work and check out which configuration options we need
for the Maven mojos.

Next we will check which [Mailchimp](https://mailchimp.com) use cases we need
to support. The code derived from Pawel's work is already integrated, but is
just checked-in as is.


## Fork me!
Feel free to fork this project to adjust it to your requirements.

This project is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
