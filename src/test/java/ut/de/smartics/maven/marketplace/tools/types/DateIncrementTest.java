/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.marketplace.tools.types;

import static org.junit.Assert.assertEquals;

import de.smartics.maven.marketplace.tools.types.DateIncrement;

import org.junit.Test;

/**
 * Tests {link {@link DateIncrement}.
 */
public class DateIncrementTest {
  //********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  @Test
  public void acceptsDays() {
    final DateIncrement uut = DateIncrement.fromString("-4d");
    assertEquals(0, uut.getYears());
    assertEquals(0, uut.getMonths());
    assertEquals(0, uut.getWeeks());
    assertEquals(-4, uut.getDays());
    assertEquals(0, uut.getHours());
    assertEquals(0, uut.getMinutes());
  }

  @Test
  public void acceptsHours() {
    final DateIncrement uut = DateIncrement.fromString("-8h");
    assertEquals(0, uut.getYears());
    assertEquals(0, uut.getMonths());
    assertEquals(0, uut.getWeeks());
    assertEquals(0, uut.getDays());
    assertEquals(-8, uut.getHours());
    assertEquals(0, uut.getMinutes());
  }

  // --- tests ----------------------------------------------------------------
}
