/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.marketplace.client.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.client.model.Licenses;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.impl.JsonEntityEncoding;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Tests parsing {@link Licenses} from a JSON file
 */
public class LicensesParserTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void parsesFile() throws IOException, MpacException {
    try (final InputStream in =
        LicensesParserTest.class.getResourceAsStream("sample-licenses.json")) {
      final JsonEntityEncoding json = new JsonEntityEncoding();

      final Licenses licenses = json.decode(in, Licenses.class);

      assertEquals(1, licenses.getLicenses().size());
      final License license = licenses.getLicenses().get(0);
      assertNotNull(license);
    }
  }
}
