/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.tools.types;

import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo.BaseMarketplaceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Helper to pretty print properties of an object.
 */
public class PrettyPrinter {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Map<String, String> properties = new LinkedHashMap<>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public interface Prettifiable {
    String toPretty();

    default String toDebug() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.MULTI_LINE_STYLE);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public void addProperty(final String name, final String value) {
    if (StringUtils.isNotEmpty(value)) {
      properties.put(name, value);
    }
  }

  // --- business -------------------------------------------------------------

  public static String createMessage(
      final BaseMarketplaceContext executionContext, final String baseMessage,
      final License license) {
    final boolean verbose = executionContext.isVerbose();
    final boolean debug = executionContext.isDebug();
    return baseMessage + license.getLicenseId()
        + (verbose ? (debug ? ": " + license : ": " + license.toPretty())
            : ".");
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final int maxNameLength = maxNameLength() + 3;

    final StringBuilder buffer = new StringBuilder(512);
    for (final Entry<String, String> entry : properties.entrySet()) {
      final String label = StringUtils.leftPad(entry.getKey(), maxNameLength);
      buffer.append('\n').append(label).append(": ").append(entry.getValue());
    }
    return buffer.toString();
  }

  private int maxNameLength() {
    int max = 0;
    for (final String name : properties.keySet()) {
      final int length = name.length();
      if (length > max) {
        max = length;
      }
    }
    return max;
  }
}
