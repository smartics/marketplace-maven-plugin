/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.tools.types;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * Specify adjustments to an instance of time.
 * <p>
 * The following format specifies the positive or negative increment.
 * </p>
 *
 * <pre>
 * {+|-}?{amount}?{unit}
 * </pre>
 * <p>
 * Use the '-' sign to go back in time, no sign or '+' to go to a future
 * instance of time.
 * </p>
 * <p>
 * If no amount is specified, a value of <tt>1</tt> is the default.
 * </p>
 * <p>
 * The following units are defined:
 * </p>
 * <table>
 * <tr>
 * <th>Symbol</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>m</td>
 * <td>Minutes</td>
 * </tr>
 * <tr>
 * <td>h</td>
 * <td>Hours</td>
 * </tr>
 * <tr>
 * <td>d</td>
 * <td>Days</td>
 * </tr>
 * <tr>
 * <td>w</td>
 * <td>Weeks</td>
 * </tr>
 * <tr>
 * <td>M</td>
 * <td>Months</td>
 * </tr>
 * <tr>
 * <td>y</td>
 * <td>Years</td>
 * </tr>
 * </table>
 * <p>
 * Examples:
 * </p>
 * <ul>
 * <li><tt>-1d</tt> - subtract one day</li>
 * <li><tt>+1d-10m</tt> - add one day and subtract 10 minutes</li>
 * <li><tt>d-10m</tt> - add one day and subtract 10 minutes</li>
 * </ul>
 */
public class DateIncrement {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final int minutes;

  private final int hours;

  private final int days;

  private final int weeks;

  private final int months;

  private final int years;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private DateIncrement(final Parser parser) {
    this.minutes = parser.minutes;
    this.hours = parser.hours;
    this.days = parser.days;
    this.weeks = parser.weeks;
    this.months = parser.months;
    this.years = parser.years;
  }

  // ****************************** Inner Classes *****************************

  private static final class Parser {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final String specification;

    private final int length;

    private int index;

    private int minutes;

    private int hours;

    private int days;

    private int weeks;

    private int months;

    private int years;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private Parser(final String specification) {
      this.specification = specification;
      this.length = specification.length();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    public static Parser parse(final String specification) {
      final Parser parser = new Parser(specification);
      parser.parse();
      return parser;
    }

    private void parse() {
      for (; index < length; index++) {
        final char currentChar = specification.charAt(index);
        switch (currentChar) {
          case '+':
          case '-':
            parseUnit(parseDigits(currentChar, ++index));
            break;
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            parseUnit(parseDigits('+', index));
            break;
          case ' ':
            break;
          case 'm':
            minutes = 1;
            break;
          case 'h':
            hours = 1;
            break;
          case 'd':
            days = 1;
            break;
          case 'w':
            weeks = 1;
            break;
          case 'M':
            months = 1;
            break;
          case 'y':
            years = 1;
            break;
          default:
            throw new IllegalArgumentException(
                "Illegal increment specification: " + specification);
        }
      }
    }

    private int parseDigits(final char sign, final int start) {
      for (; index < length; index++) {
        final char currentChar = specification.charAt(index);
        if (!Character.isDigit(currentChar)) {
          if (index > start) {
            final String digitString = specification.substring(start, index);
            final int digits = Integer.parseInt(digitString);
            return sign == '-' ? -digits : digits;
          }
          return sign == '-' ? -1 : 1;
        }
      }
      return sign == '-' ? -1 : 1;
    }

    private void parseUnit(final int count) {
      for (; index < length; index++) {
        final char currentChar = specification.charAt(index);
        switch (currentChar) {
          case ' ':
            break;
          case 'm':
            minutes = count;
            return;
          case 'h':
            hours = count;
            return;
          case 'd':
            days = count;
            return;
          case 'w':
            weeks = count;
            return;
          case 'M':
            months = count;
            return;
          case 'y':
            years = count;
            return;
          default:
            throw new IllegalArgumentException("Illegal unit '" + currentChar
                + "' in increment specification: " + specification);
        }
      }
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static final DateIncrement fromString(final String specification) {
    final Parser parser = Parser.parse(specification);
    return new DateIncrement(parser);
  }

  // --- get&set --------------------------------------------------------------

  public int getMinutes() {
    return minutes;
  }

  public int getHours() {
    return hours;
  }

  public int getDays() {
    return days;
  }

  public int getWeeks() {
    return weeks;
  }

  public int getMonths() {
    return months;
  }

  public int getYears() {
    return years;
  }

  // --- business -------------------------------------------------------------

  public LocalDate apply(final LocalDate date) {
    return apply(date.toDateTimeAtCurrentTime()).toLocalDate();
  }

  public DateTime apply(final DateTime date) {
    DateTime currentDate = date;
    if (minutes != 0) {
      currentDate = currentDate.plusMinutes(minutes);
    }
    if (hours != 0) {
      currentDate = currentDate.plusHours(hours);
    }
    if (days != 0) {
      currentDate = currentDate.plusDays(days);
    }
    if (weeks != 0) {
      currentDate = currentDate.plusWeeks(weeks);
    }
    if (months != 0) {
      currentDate = currentDate.plusMonths(months);
    }
    if (years != 0) {
      currentDate = currentDate.plusYears(years);
    }

    return currentDate;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.SHORT_PREFIX_STYLE);
  }
}
