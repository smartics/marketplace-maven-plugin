/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.tools.maven;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.settings.Profile;
import org.apache.maven.settings.Settings;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Provides access to properties provided by the Maven settings file.
 */
public class SettingsPropertiesManger {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private final Properties properties;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SettingsPropertiesManger(final MavenSession session) {
    properties = init(session);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private static Properties init(final MavenSession session) {
    final Properties properties = new Properties();
    if (session != null) {
      final Settings settings = session.getSettings();
      if (settings != null) {
        final Map<String, Profile> profiles = settings.getProfilesAsMap();
        final List<String> activeProfileNames = settings.getActiveProfiles();
        for (final String activeProfileName : activeProfileNames) {
          final Profile activeProfile = profiles.get(activeProfileName);
          final Properties activeProperties = activeProfile.getProperties();
          properties.putAll(activeProperties);
        }
      }
    }

    return properties;
  }

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getProperty(final String key) {
    return properties.getProperty(key);
  }

  public String getProperty(final String key, final String defaultValue) {
    return properties.getProperty(key, defaultValue);
  }

  public boolean containsKey(final String key) {
    return properties.containsKey(key);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
