/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.tools.types;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.atlassian.fugue.Option;

/**
 * Provides helpers to calculate dates relative to a specified current date.
 */
public final class TimeMachine {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final DateTime now;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private TimeMachine(final DateTime now) {
    this.now = now;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static TimeMachine create(@Nullable final DateTime now) {
    return new TimeMachine(now != null ? now : DateTime.now());
  }

  public static TimeMachine create(final String pattern, final String now)
      throws IllegalArgumentException {
    if (StringUtils.isBlank(now)) {
      return create(DateTime.now());
    } else {
      final DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
      final DateTime nowDate = formatter.parseDateTime(now);
      return create(nowDate);
    }
  }

  public static TimeMachine create(final String now)
      throws IllegalArgumentException {
    if (StringUtils.isBlank(now)) {
      return create(DateTime.now());
    } else {
      final DateTimeFormatter formatter = ISODateTimeFormat.dateTimeParser();
      final DateTime nowDate = formatter.parseDateTime(now);
      return create(nowDate);
    }
  }

  // --- get&set --------------------------------------------------------------

  public LocalDate getNowLocal() {
    return now.toLocalDate();
  }

  // --- business -------------------------------------------------------------

  public static Option<LocalDate> relativeToNow(
      @Nullable final String incrementPattern) {
    return new TimeMachine(DateTime.now()).calcDate(incrementPattern);
  }

  public Option<LocalDate> calcDate() {
    return internalCalcDate(now, null);
  }

  public Option<LocalDate> calcDate(@Nullable final String incrementPattern) {
    return internalCalcDate(now, incrementPattern);
  }

  public Option<LocalDate> calcDate(@Nullable final DateTime pointInTime,
      @Nullable final String incrementPattern) {
    return internalCalcDate(pointInTime != null ? pointInTime : now,
        incrementPattern);
  }

  private Option<LocalDate> internalCalcDate(
      @Nonnull final DateTime pointInTime,
      @Nullable final String incrementPattern) {
    final Option<LocalDate> calcDate;
    if (StringUtils.isNotBlank(incrementPattern)) {
      final DateIncrement increment =
          DateIncrement.fromString(incrementPattern);
      calcDate = some(increment.apply(pointInTime).toLocalDate());
    } else {
      calcDate = none();
    }
    return calcDate;
  }

  // --- object basics --------------------------------------------------------

}
