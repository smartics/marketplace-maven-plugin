/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.services.methods;

import com.ecwid.maleorang.MailchimpMethod;
import com.ecwid.maleorang.MailchimpObject;
import com.ecwid.maleorang.annotation.APIVersion;
import com.ecwid.maleorang.annotation.Field;
import com.ecwid.maleorang.annotation.HttpMethod;
import com.ecwid.maleorang.annotation.Method;
import com.ecwid.maleorang.annotation.PathParam;
import com.ecwid.maleorang.method.v3_0.lists.members.MemberInfo;
import kotlin.jvm.JvmField;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Method to post interests to a list member. List members are identified by the
 * identifier of the list and their email address.
 *
 * @see
 * <a href="https://mailchimp.com/developer/reference/lists/interest-categories/interests/">Interests</a>
 * - Manage interests for a specific Mailchimp list. Assign subscribers to
 * interests to group them together. Interests are referred to as 'group names'
 * in the Mailchimp application.
 */
public class ListMemberInterestsMethod extends MailchimpMethod<MemberInfo> {
  @Method(httpMethod = HttpMethod.POST, version = APIVersion.v3_0, path =
      "/lists/{list_id}/members/{subscriber_hash}/interests")

  // FIXME: Map interest to subscriber

  public static class PostInterestsToListMember extends ListMemberInterestsMethod {
    @JvmField
    @PathParam(name = "list_id")
    private final String listId;

    @JvmField
    @PathParam(name = "subscriber_hash")
    private final String subscriberHash;

    @JvmField
    @Field
    private MailchimpObject interests;

    public PostInterestsToListMember(final String listId,
        final String emailAddress) {
      this.listId = listId;
      this.subscriberHash = DigestUtils.md5Hex(emailAddress.toLowerCase());
    }

    public static final class Interest extends MailchimpObject {
      @JvmField
      @Field
      private final String name;
      @JvmField
      @Field
      private final boolean status;

      public Interest(final String name, final boolean status) {
        this.name = name;
        this.status = status;
      }

      public static List<Interest> activateInterests(
          final String... interestNames) {
        final List<Interest> interests = new ArrayList<>(interestNames.length);
        return activateInterests(interests, interestNames);
      }

      public static List<Interest> activateInterests(
          final List<Interest> interests, final String... interestNames) {
        return interest(interests, interestNames, true);
      }

      public static List<Interest> deactivateInterests(
          final String... interestNames) {
        final List<Interest> interests = new ArrayList<>(interestNames.length);
        return deactivateInterests(interests, interestNames);
      }

      public static List<Interest> deactivateInterests(
          final List<Interest> interests, final String... interestNames) {
        return interest(interests, interestNames, false);
      }

      private static List<Interest> interest(final List<Interest> interests,
          final String[] interestNames, final boolean status) {
        for (final String interestName : interestNames) {
          final Interest interest = new Interest(interestName, status);
          interests.add(interest);
        }
        return interests;
      }
    }

    public void putInterests(final List<Interest> interests) {
      this.interests = new MailchimpObject();
      for(final Interest interest : interests) {
        this.interests.mapping.put(interest.name, interest.status);
      }
    }
  }
}
