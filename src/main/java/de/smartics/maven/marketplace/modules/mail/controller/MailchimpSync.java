/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.controller;

import static de.smartics.maven.marketplace.tools.types.PrettyPrinter.createMessage;

import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView;
import de.smartics.maven.marketplace.modules.mail.domain.MailContact;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView.MissingEmailException;
import de.smartics.maven.marketplace.modules.mail.services.CreateOrUpdateListMember;
import de.smartics.maven.marketplace.modules.mail.services.GetListMember;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListClient;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListMember;
import de.smartics.maven.marketplace.modules.market.controller.TransitionRules;
import de.smartics.maven.marketplace.modules.market.controller.TransitionRules.TransitionContext;
import de.smartics.maven.marketplace.modules.market.controller.TransitionRules.TransitionProcessor;
import de.smartics.maven.marketplace.modules.market.domain.LicenseState;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo.BaseMarketplaceContext;
import de.smartics.maven.marketplace.tools.types.DateIncrement;

import com.ecwid.maleorang.MailchimpException;

import org.joda.time.LocalDate;

import java.io.IOException;

import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;

public class MailchimpSync implements LicenseProcessor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final TransitionRules transitionRules;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public MailchimpSync(final BaseMarketplaceContext executionContext,
      final MailchimpListClient client, final LocalDate now,
      final DateIncrement beforeEvaluationEnd) {
    this.transitionRules = TransitionRules
        .create(executionContext, now, beforeEvaluationEnd,
            new MailchimpTransitionProcessor(executionContext, client));
  }

  // ****************************** Inner Classes *****************************


  private static final class MailchimpTransitionProcessor
      implements TransitionProcessor {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final BaseMarketplaceContext executionContext;

    private final GetListMember getService;

    private final CreateOrUpdateListMember updateService;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private MailchimpTransitionProcessor(
        final BaseMarketplaceContext executionContext,
        final MailchimpListClient client) {
      this.executionContext = executionContext;
      final boolean dryRun = executionContext.isDryRun();
      getService = new GetListMember(client);
      updateService = new CreateOrUpdateListMember(executionContext,
          dryRun ? null : client);
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    @Override
    public BaseMarketplaceContext getExecutionContext() {
      return executionContext;
    }

    // --- business -----------------------------------------------------------

    @Override
    public Either<Exception, String> process(final TransitionContext context) {
      try {
        final License license = context.getLicense();
        final Option<MailContact> contact = MailContact.from(license);
        if (contact.isDefined()) {
          final LicenseMailView view =
              LicenseMailView.from(contact.get(), license);
          final String email = view.getEmail();

          final LicenseState targetState = context.getTargetState();
          switch (targetState) {
            case FREE_TRIAL_STARTED:
              return handleRequest(license, contact, view, email, targetState);
            case FREE_TRIAL_ABOUT_TO_END:
              return handleRequest(license, contact, view, email, targetState);
            case FREE_TRIAL_ENDED:
              return Either.right(createMessage(executionContext,
                  "Evaluation already ended for ", license));
            case LICENSE_ACQUIRED: {
              return handleRequest(license, contact, view, email, targetState);
            }
            default:
              throw new IllegalArgumentException(
                  "Unknown target state: " + targetState);
          }
        } else {
          return Either.left(MissingEmailException.from(license));
        }
      } catch (final Exception e) {
        return Either.left(e);
      }
    }

    private Either<Exception, String> handleRequest(final License license,
        final Option<MailContact> contact, final LicenseMailView view,
        final String email, final LicenseState targetState)
        throws IOException, MailchimpException {
      final Option<MailchimpListMember> member = getService.execute(email);
      final String targetStateTag = targetState.getLabel();
      if (member.isDefined() && member.getOrNull().hasTag(targetStateTag)) {
        return Either.right(createMessage(executionContext,
            targetStateTag + ": Message has already been sent to ", license));
      } else {
        updateService.execute(license, contact, view, email, targetState);
        return Either.right(createMessage(executionContext, targetStateTag +
                                                            ": Evaluation End" +
                                                            " Reminder " +
                                                            "Message has NOW " +
                                                            "been sent to ",
            license));
      }
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Either<Exception, String> process(final License license) {
    return transitionRules.process(license);
  }

  // --- object basics --------------------------------------------------------

}
