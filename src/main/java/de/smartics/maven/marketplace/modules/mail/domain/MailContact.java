/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.domain;

import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.client.model.License.Contact;
import de.smartics.maven.marketplace.client.model.License.ContactDetails;
import de.smartics.maven.marketplace.client.model.License.PartnerDetails;

import io.atlassian.fugue.Option;

/**
 * Information of a contact to send an email.
 */
public class MailContact {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private final Option<String> companyName;

  private final Option<String> name;

  private final String email;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private MailContact(final Option<String> companyName,
      final Option<String> name, final String email) {
    this.companyName = companyName;
    this.name = name;
    this.email = email;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  /**
   * Contact information is required to have an email address provided. The
   * first contact with an email address is returned. The model allows to have
   * contacts without an email address. In this case none is returned.
   *
   * @param license the license with contact information.
   * @return the first contact in order billing, technical, partner, with an
   *         email address.
   */
  public static Option<MailContact> from(final License license) {
    final ContactDetails contactDetails = license.getContactDetails();

    final Option<Contact> possibleBillingContact =
        contactDetails.getBillingContact();
    if (possibleBillingContact.isDefined()) {
      final Contact contact = possibleBillingContact.get();
      final Option<String> contactEmail = contact.getEmail();
      if (contactEmail.isDefined()) {
        return constructOption(contactDetails, contact, contactEmail);
      }
    }

    final Option<Contact> possibleTechnicalContact =
        contactDetails.getTechnicalContact();
    if (possibleTechnicalContact.isDefined()) {
      final Contact contact = possibleTechnicalContact.get();
      final Option<String> contactEmail = contact.getEmail();
      if (contactEmail.isDefined()) {
        return constructOption(contactDetails, contact, contactEmail);
      }
    }

    final Option<PartnerDetails> possiblePartnerDetails =
        license.getPartnerDetails();
    if (possiblePartnerDetails.isDefined()) {
      final PartnerDetails partnerDetails = possiblePartnerDetails.get();
      final Option<Contact> possiblePartnerContact =
          partnerDetails.getBillingContact();
      if (possiblePartnerContact.isDefined()) {
        final Contact contact = possiblePartnerContact.get();
        final Option<String> contactEmail = contact.getEmail();
        if (contactEmail.isDefined()) {
          return constructOption(contactDetails, contact, contactEmail);
        }
      }
    }

    return Option.none();
  }

  private static Option<MailContact> constructOption(
      final ContactDetails contactDetails, final Contact contact,
      final Option<String> contactEmail) {
    final String email = contactEmail.get();
    final Option<String> companyName = Option.some(contactDetails.getCompany());
    final Option<String> contactName = contact.getName();
    return Option.option(new MailContact(companyName, contactName, email));
  }

  // --- get&set --------------------------------------------------------------

  public Option<String> getCompanyName() {
    return companyName;
  }

  public Option<String> getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return email + (name.isDefined() ? ": " + name.get() : "")
        + (companyName.isDefined() ? " (" + companyName.get() + ")" : "");
  }
}
