/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.market.controller;

import static de.smartics.maven.marketplace.tools.types.PrettyPrinter.createMessage;
import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.api.LicenseType;
import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.modules.market.domain.LicenseState;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo.BaseMarketplaceContext;
import de.smartics.maven.marketplace.tools.types.DateIncrement;

import org.joda.time.LocalDate;

import io.atlassian.fugue.Either;

/**
 * Handles transitions of license states.
 */
public final class TransitionRules implements LicenseProcessor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final BaseMarketplaceContext executionContext;

  private final LocalDate now;

  private final DateIncrement beforeEvaluationEnd;

  private final TransitionProcessor processor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private TransitionRules(final BaseMarketplaceContext executionContext,
      final LocalDate now, final DateIncrement beforeEvaluationEnd,
      final TransitionProcessor processor) {
    this.executionContext = executionContext;
    this.now = now;
    this.beforeEvaluationEnd = beforeEvaluationEnd;
    this.processor =
        processor != null ? processor : new DefaultTransitionProcessor();
  }

  // ****************************** Inner Classes *****************************

  private final class DefaultTransitionProcessor
      implements TransitionProcessor {
    @Override
    public BaseMarketplaceContext getExecutionContext() {
      return executionContext;
    }

  }

  public interface TransitionProcessor {
    // ********************************* Fields *******************************

    // --- constants ----------------------------------------------------------

    // ****************************** Initializer *****************************

    // ****************************** Inner Classes ***************************

    // ********************************* Methods ******************************

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    BaseMarketplaceContext getExecutionContext();

    default Either<Exception, String> process(final TransitionContext context) {
      final License license = context.getLicense();
      switch (context.targetState) {
        case FREE_TRIAL_STARTED:
          return Either.right(createMessage(getExecutionContext(),
              "Evaluation Start message may be required for ", license));
        case FREE_TRIAL_ABOUT_TO_END:
          return Either.right(createMessage(getExecutionContext(),
              "Evaluation end reminder may be required for ", license));
        case FREE_TRIAL_ENDED:
          return Either.right(createMessage(getExecutionContext(),
              "Evaluation already ended for ", license));
        case LICENSE_ACQUIRED:
          return Either.right(createMessage(getExecutionContext(),
              "Acquired License message may be required for ", license));
        default:
          throw new IllegalArgumentException(
              "Unknown target state: " + context.targetState);
      }
    }

    // --- object basics ------------------------------------------------------
  }

  public static final class TransitionContext {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    private final License license;

    private final LicenseState targetState;

    // --- members ------------------------------------------------------------

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private TransitionContext(final License license,
        final LicenseState targetState) {
      this.license = license;
      this.targetState = targetState;
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    public License getLicense() {
      return license;
    }

    public LicenseState getTargetState() {
      return targetState;
    }

    // --- business -----------------------------------------------------------

    public boolean isStateUpdateRequired(final LicenseState currentState) {
      if (!currentState.isEndState()) {
        return targetState != currentState;
      }
      return false;
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static TransitionRules create(
      final BaseMarketplaceContext executionContext, final LocalDate now,
      final DateIncrement beforeEvaluationEnd) {
    return create(executionContext, now, beforeEvaluationEnd, null);
  }

  public static TransitionRules create(
      final BaseMarketplaceContext executionContext, final LocalDate now,
      final DateIncrement beforeEvaluationEnd,
      final TransitionProcessor processor) {
    return new TransitionRules(executionContext, now, beforeEvaluationEnd,
        processor);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Either<Exception, String> process(final License license) {
    try {
      final LicenseType licenseType =
          LicenseType.fromString(license.getLicenseType());
      final LocalDate maintainanceEnd = license.getMaintenanceEndDate();
      switch (licenseType) {
        case EVALUATION:
          if (maintainanceEnd.isBefore(now)) {
            return processor.process(
                new TransitionContext(license, LicenseState.FREE_TRIAL_ENDED));
          } else {
            final LocalDate beforeEvaluation =
                beforeEvaluationEnd.apply(maintainanceEnd);
            if (!now.isBefore(beforeEvaluation)) {
              return processor.process(new TransitionContext(license,
                  LicenseState.FREE_TRIAL_ABOUT_TO_END));
            } else {
              return processor.process(new TransitionContext(license,
                  LicenseState.FREE_TRIAL_STARTED));
            }
          }
        case COMMERCIAL:
        case ACADEMIC:
        case COMMUNITY:
        case OPEN_SOURCE:
          if (maintainanceEnd.isBefore(now)) {
            return Either.right(createMessage(executionContext,
                "Maintenance already ended for ", license));
          } else {
            final LocalDate maintainanceStart =
                license.getMaintenanceStartDate();
            if (!maintainanceStart.isBefore(now)) {
              return processor.process(new TransitionContext(license,
                  LicenseState.LICENSE_ACQUIRED));
            } else {
              return Either.right(createMessage(executionContext,
                  "Nothing to do for ", license));
            }
          }
        case DEMONSTRATION:
        default:
          return Either.right(createMessage(executionContext,
              "No handling of license type " + licenseType + " for ", license));
      }

    } catch (final RuntimeException e) {
      return Either.left(e);
    }
  }

  // --- object basics --------------------------------------------------------

}
