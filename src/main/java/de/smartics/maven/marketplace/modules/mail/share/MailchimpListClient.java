/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.share;

import com.ecwid.maleorang.MailchimpClient;
import com.ecwid.maleorang.MailchimpException;
import com.ecwid.maleorang.MailchimpMethod;
import com.ecwid.maleorang.MailchimpObject;
import com.ecwid.maleorang.method.v3_0.lists.members.EditMemberMethod;
import com.ecwid.maleorang.method.v3_0.lists.members.EditMemberMethod.CreateOrUpdate;
import com.ecwid.maleorang.method.v3_0.lists.members.GetMemberMethod;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import de.smartics.maven.marketplace.modules.mail.services.methods.ListMemberTagsMethod.PostTagsToListMember;

import java.io.IOException;

/**
 * Base configuration of a REST client to the Mailchimp API based on a
 * list/audience.
 */
public class MailchimpListClient {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Supplier<MailchimpClient> mailchimpClient;

  private final String listId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Supplier<MailchimpClient> getMailchimpClient() {
    return mailchimpClient;
  }

  public MailchimpListClient(final String apiKey, final String listId) {
    mailchimpClient = Suppliers.memoize(() -> new MailchimpClient(apiKey));
    this.listId = listId;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getListId() {
    return listId;
  }

  // --- business -------------------------------------------------------------

  public <R extends MailchimpObject> R execute(final MailchimpMethod<R> method)
      throws IOException, MailchimpException {
    return mailchimpClient.get().execute(method);
  }

  public CreateOrUpdate createMethodMemberCreateOrUpdate(final String email) {
    return new EditMemberMethod.CreateOrUpdate(listId, email);
  }

  public GetMemberMethod createMethodMemberGet(final String email) {
    return new GetMemberMethod(listId, email);
  }

  public PostTagsToListMember createPostTagsToListMember(final String email) {
    return new PostTagsToListMember(listId, email);
  }

  // --- object basics --------------------------------------------------------

}
