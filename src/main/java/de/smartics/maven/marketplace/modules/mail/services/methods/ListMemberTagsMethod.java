/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.services.methods;

import com.ecwid.maleorang.MailchimpMethod;
import com.ecwid.maleorang.MailchimpObject;
import com.ecwid.maleorang.annotation.APIVersion;
import com.ecwid.maleorang.annotation.Field;
import com.ecwid.maleorang.annotation.HttpMethod;
import com.ecwid.maleorang.annotation.Method;
import com.ecwid.maleorang.annotation.PathParam;
import com.ecwid.maleorang.method.v3_0.lists.members.EditMemberMethod;
import com.ecwid.maleorang.method.v3_0.lists.members.MemberInfo;
import kotlin.jvm.JvmField;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Method to post tags to a list member. List members are identified by the
 * identifier of the list and their email address.
 *
 * @see
 * <a href="https://mailchimp.com/developer/reference/lists/list-members/list-member-tags/">Post
 * the tags on a list member</a>
 */
public class ListMemberTagsMethod extends MailchimpMethod<MemberInfo> {
  @Method(httpMethod = HttpMethod.POST, version = APIVersion.v3_0, path =
      "/lists/{list_id}/members/{subscriber_hash}/tags")
  public static class PostTagsToListMember extends ListMemberTagsMethod {
    @JvmField
    @PathParam(name = "list_id")
    private final String listId;

    @JvmField
    @PathParam(name = "subscriber_hash")
    private final String subscriberHash;

    @JvmField
    @Field(name = "tags")
    private List<Tag> tags;

    public PostTagsToListMember(final String listId,
        final String emailAddress) {
      this.listId = listId;
      this.subscriberHash = DigestUtils.md5Hex(emailAddress.toLowerCase());
    }

    public static final class Tag extends MailchimpObject {
      @JvmField
      @Field
      private final String name;
      @JvmField
      @Field
      private final String status;

      public Tag(final String name, final String status) {
        this.name = name;
        this.status = status;
      }

      public static List<Tag> activateTags(final String... tagNames) {
        final List<Tag> tags = new ArrayList<>(tagNames.length);
        return activateTags(tags, tagNames);
      }

      public static List<Tag> activateTags(final List<Tag> tags,
          final String... tagNames) {
        return tag(tags, tagNames, "active");
      }

      public static List<Tag> deactivateTags(final String... tagNames) {
        final List<Tag> tags = new ArrayList<>(tagNames.length);
        return deactivateTags(tags, tagNames);
      }

      public static List<Tag> deactivateTags(final List<Tag> tags,
          final String... tagNames) {
        return tag(tags, tagNames, "inactive");
      }

      private static List<Tag> tag(final List<Tag> tags,
          final String[] tagNames, final String status) {
        for (final String tagName : tagNames) {
          final Tag tag = new Tag(tagName, status);
          tags.add(tag);
        }
        return tags;
      }
    }

    public void putTags(final List<Tag> tags) {
      this.tags = tags;
    }
  }
}
