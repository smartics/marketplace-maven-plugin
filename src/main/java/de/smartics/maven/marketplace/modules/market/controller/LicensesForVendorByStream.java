/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.market.controller;

import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.api.VendorSource;
import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.client.model.Licenses;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.impl.JsonEntityEncoding;
import com.google.common.collect.Lists;

import java.io.InputStream;
import java.util.List;

import io.atlassian.fugue.Either;

/**
 * Processes licenses provided by a stream.
 */
public class LicensesForVendorByStream implements VendorSource {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Licenses licenses;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private LicensesForVendorByStream(final Licenses licenses) {
    this.licenses = licenses;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static LicensesForVendorByStream create(final InputStream in)
      throws MpacException {
    final JsonEntityEncoding json = new JsonEntityEncoding();
    final Licenses licenses = json.decode(in, Licenses.class);
    return new LicensesForVendorByStream(licenses);
  }

  public static LicensesForVendorByStream create(final Licenses licenses) {
    return new LicensesForVendorByStream(licenses);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void processWith(final LicenseProcessor processor) throws MpacException {
    final List<Either<Exception, String>> results = Lists.newArrayList();
    for (final License license : licenses) {
      results.add(processor.process(license));
    }

    processor.evaluate(results);
  }

  // --- object basics --------------------------------------------------------

}
