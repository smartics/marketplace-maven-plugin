/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.domain;

import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.share.domain.AppException;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDate;

import io.atlassian.fugue.Option;

/**
 * Provides a view on a license with information relevant for mail delivery. It
 * is assured that the email address is present.
 */
public class LicenseMailView {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String licenseId;
  private final String licenseType;
  private final String status;
  private final String tier;
  private final LocalDate startDate;
  private final LocalDate endDate;
  private final String email;
  private final Option<String> contactName;
  private final Option<String> companyName;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private LicenseMailView(final String licenseId, final String licenseType,
      final String status, final String tier, final LocalDate startDate,
      final LocalDate endDate, final MailContact contact) {
    this.licenseId = licenseId;
    this.licenseType = licenseType;
    this.status = status;
    this.tier = tier;
    this.startDate = startDate;
    this.endDate = endDate;
    this.email = contact.getEmail();
    this.contactName = contact.getName();
    this.companyName = contact.getCompanyName();
  }

  // ****************************** Inner Classes *****************************

  public static final class MissingEmailException extends AppException {
    private static final long serialVersionUID = 1L;

    private final String licenseId;
    private final String licenseType;
    private final String status;
    private final String tier;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public MissingEmailException(final License license) {
      super("Email address for contact is missing.");
      this.licenseId = license.getLicenseId();
      this.licenseType = license.getLicenseType();
      this.status = license.getStatus();
      this.tier = license.getTier();
      this.startDate = license.getMaintenanceStartDate();
      this.endDate = license.getMaintenanceEndDate();
    }

    public static MissingEmailException from(final License license) {
      return new MissingEmailException(license);
    }

    public String getLicenseId() {
      return licenseId;
    }

    public String getLicenseType() {
      return licenseType;
    }

    public String getStatus() {
      return status;
    }

    public String getTier() {
      return tier;
    }

    public LocalDate getStartDate() {
      return startDate;
    }

    public LocalDate getEndDate() {
      return endDate;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.MULTI_LINE_STYLE);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static LicenseMailView from(final MailContact contact,
      final License license) {
    final String licenseId = license.getLicenseId();
    final String licenseType = license.getLicenseType();
    final String status = license.getStatus();
    final String tier = license.getTier();
    final LocalDate startDate = license.getMaintenanceStartDate();
    final LocalDate endDate = license.getMaintenanceEndDate();

    return new LicenseMailView(licenseId, licenseType, status, tier, startDate,
        endDate, contact);
  }

  // --- get&set --------------------------------------------------------------

  public String getLicenseId() {
    return licenseId;
  }

  public String getLicenseType() {
    return licenseType;
  }

  public String getStatus() {
    return status;
  }

  public String getTier() {
    return tier;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public String getEmail() {
    return email;
  }

  public Option<String> getContactName() {
    return contactName;
  }

  public Option<String> getCompanyName() {
    return companyName;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
