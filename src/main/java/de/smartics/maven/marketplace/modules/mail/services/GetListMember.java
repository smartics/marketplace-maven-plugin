/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.services;

import de.smartics.maven.marketplace.modules.mail.share.MailchimpListClient;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListMember;

import com.ecwid.maleorang.MailchimpException;
import com.ecwid.maleorang.method.v3_0.lists.members.GetMemberMethod;
import com.ecwid.maleorang.method.v3_0.lists.members.MemberInfo;
import io.atlassian.fugue.Option;

import java.io.IOException;

public class GetListMember {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MailchimpListClient client;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public GetListMember(final MailchimpListClient client) {
    this.client = client;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public Option<MailchimpListMember> execute(final String email)
      throws IOException {
    final GetMemberMethod method = client.createMethodMemberGet(email);
    try {
      final MemberInfo info = client.execute(method);
      final MailchimpListMember member = MailchimpListMember.from(info);
      return Option.option(member);
    }
    catch(final MailchimpException e)
    {
      return Option.none();
    }
  }

  // --- object basics --------------------------------------------------------

}
