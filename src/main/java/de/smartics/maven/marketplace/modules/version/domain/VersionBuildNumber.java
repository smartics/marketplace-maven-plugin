/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.version.domain;

import javax.annotation.Nullable;

/**
 * Information about the version and its build number.
 */
public class VersionBuildNumber {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Nullable
  private final String version;

  @Nullable
  private final Integer buildNumber;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public VersionBuildNumber(@Nullable final String version,
      @Nullable final Integer buildNumber) {
    this.version = version;
    this.buildNumber = buildNumber;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Nullable
  public String getVersion() {
    return version;
  }

  @Nullable
  public Integer getBuildNumber() {
    return buildNumber;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return (version != null ? version : "n/a")
        + (buildNumber != null ? " (" + buildNumber + ')' : "");
  }
}
