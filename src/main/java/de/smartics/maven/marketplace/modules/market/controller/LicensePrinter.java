/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.market.controller;

import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView;
import de.smartics.maven.marketplace.modules.mail.domain.MailContact;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView.MissingEmailException;

import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;

/**
 * Returns the string representation of the license.
 */
public class LicensePrinter implements LicenseProcessor {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Either<Exception, String> process(final License license)
      throws RuntimeException {

    final Option<MailContact> contact = MailContact.from(license);
    if (contact.isDefined()) {
      return Either
          .right(LicenseMailView.from(contact.get(), license).toString());
    } else {
      return Either.left(MissingEmailException.from(license));
    }
  }

  // --- object basics --------------------------------------------------------

}
