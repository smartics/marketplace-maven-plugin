/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.market.domain;

/**
 * Lists states of a license journey
 */
public enum LicenseState {
  // ***************************** Enumeration ******************************

  // ******************************** Fields ********************************

  // --- constants ----------------------------------------------------------

  FREE_TRIAL_STARTED("Free Trial Started"),

  FREE_TRIAL_ABOUT_TO_END("Free Trial About To End"),

  FREE_TRIAL_ENDED("Free Trial Ended", true),

  LICENSE_ACQUIRED("License Acquired");

  // --- members ------------------------------------------------------------

  private final String label;

  private final boolean endState;

  // ***************************** Constructors *****************************

  LicenseState(final String label) {
    this(label, false);
  }

  LicenseState(final String label, final boolean endState) {
    this.label = label;
    this.endState = endState;
  }

  // ******************************** Methods *******************************

  // --- init ---------------------------------------------------------------

  // --- get&set ------------------------------------------------------------

  public String getLabel() {
    return label;
  }

  public boolean isEndState() {
    return endState;
  }

  // --- business -----------------------------------------------------------

  // --- object basics ------------------------------------------------------

  @Override
  public String toString() {
    return label;
  }
}
