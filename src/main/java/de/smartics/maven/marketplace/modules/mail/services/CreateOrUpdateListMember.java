/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.services;

import static de.smartics.maven.marketplace.tools.types.PrettyPrinter.createMessage;

import de.smartics.maven.marketplace.client.model.License;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView;
import de.smartics.maven.marketplace.modules.mail.domain.LicenseMailView.MissingEmailException;
import de.smartics.maven.marketplace.modules.mail.domain.MailContact;
import de.smartics.maven.marketplace.modules.mail.services.methods.ListMemberTagsMethod;
import de.smartics.maven.marketplace.modules.mail.services.methods.ListMemberTagsMethod.PostTagsToListMember.Tag;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListClient;
import de.smartics.maven.marketplace.modules.market.domain.LicenseState;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo.BaseMarketplaceContext;

import com.ecwid.maleorang.MailchimpObject;
import com.ecwid.maleorang.method.v3_0.lists.members.EditMemberMethod;

import javax.annotation.Nullable;

import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class CreateOrUpdateListMember {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final BaseMarketplaceContext executionContext;

  @Nullable
  private final MailchimpListClient client;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public CreateOrUpdateListMember(final BaseMarketplaceContext executionContext,
      @Nullable final MailchimpListClient client) {
    this.executionContext = executionContext;
    this.client = client;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public Either<Exception, String> execute(final License license,
      @Nullable final LicenseState targetState) {
    if (client == null) {
      return dryRun(license);
    }

    final Option<MailContact> contact = MailContact.from(license);
    if (contact.isDefined()) {
      final LicenseMailView view = LicenseMailView.from(contact.get(), license);
      final String email = view.getEmail();
      return internalExecute(license, contact, view, email, targetState);
    } else {
      return Either.left(MissingEmailException.from(license));
    }
  }

  public Either<Exception, String> execute(final License license,
      final Option<MailContact> contact, final LicenseMailView view,
      final String email, final LicenseState targetState) {
    if (client == null) {
      return dryRun(license);
    }

    return internalExecute(license, contact, view, email, targetState);
  }

  private Either<Exception, String> dryRun(final License license) {
    return Either.right(createMessage(executionContext,
        "Due to dry-run mode execution skipped for ", license));
  }

  private Either<Exception, String> internalExecute(final License license,
      final Option<MailContact> contact, final LicenseMailView view,
      final String email, final LicenseState targetState) {
    final EditMemberMethod.CreateOrUpdate createOrUpdate =
        client.createMethodMemberCreateOrUpdate(email);
    createOrUpdate.status = "subscribed";

    createOrUpdate.merge_fields = new MailchimpObject();
    final Option<String> companyName = view.getCompanyName();
    if (companyName.isDefined()) {
      createOrUpdate.merge_fields.mapping.put("ADDRESS", companyName.get());
    }
    final Option<String> contactName = view.getContactName();
    if (contactName.isDefined()) {
      createOrUpdate.merge_fields.mapping.put("LNAME", contactName.get());
      createOrUpdate.merge_fields.mapping.put("FNAME", contactName.get());
    }

/*
      final String addonKeyEnv =
          StringUtils.replaceChars(license.getAddonKey(), "-.", "__");
      final String mailChimpInterest =
          "MAILCHIMP_INTEREST_" + addonKeyEnv;
      if (StringUtils.isNotBlank(addonKeyEnv)) {
        createOrUpdate.interests = new MailchimpObject();
        createOrUpdate.interests.mapping.put(mailChimpInterest, true);
      }
*/
    final ListMemberTagsMethod.PostTagsToListMember postTagsToListMember =
        client.createPostTagsToListMember(email);
    if (targetState != null) {
      final String newTag = targetState.getLabel();
      final List<Tag> tags = Tag.activateTags(newTag);
      postTagsToListMember.putTags(tags);
    }

    try {
      client.execute(createOrUpdate);
      try {
        client.execute(postTagsToListMember);

        return Either
            .right(LicenseMailView.from(contact.get(), license).toString());
      } catch (final Exception e) {
        return Either.left(e);
      }
    } catch (final Exception e) {
      return Either.left(e);
    }
  }

  // --- object basics --------------------------------------------------------

}
