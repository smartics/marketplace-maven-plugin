/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.market.controller;

import de.smartics.maven.marketplace.client.api.LicenseIterator;
import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.VendorSource;
import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.VendorQuery;
import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;
import com.atlassian.marketplace.client.model.VendorSummary;
import com.google.common.collect.Lists;

import java.util.List;

import io.atlassian.fugue.Either;

/**
 * Processes licenses for the logged-in vendor.
 */
public class LicensesForThisVendor implements VendorSource {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ExtendedMarketplaceClient client;

  private final LicenseQuery licenseQuery;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private LicensesForThisVendor(final ExtendedMarketplaceClient client,
      final LicenseQuery licenseQuery) {
    this.client = client;
    this.licenseQuery = licenseQuery;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static LicensesForThisVendor create(
      final ExtendedMarketplaceClient client, final LicenseQuery licenseQuery) {
    return new LicensesForThisVendor(client, licenseQuery);
  }

  public static void execute(final ExtendedMarketplaceClient client,
      final LicenseQuery licenseQuery, final LicenseProcessor processor)
      throws MpacException {
    create(client, licenseQuery).processWith(processor);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void processWith(final LicenseProcessor processor) throws MpacException {
    final VendorQuery vendorQuery =
        VendorQuery.builder().forThisUserOnly(true).build();

    final Page<VendorSummary> vendors = client.vendors().find(vendorQuery);

    final List<Either<Exception, String>> results = Lists.newArrayList();
    for (final VendorSummary vendor : vendors) {
      final LicenseIterator licenseIterator =
          new LicenseIterator(client, vendor);
      results.addAll(licenseIterator.execute(licenseQuery, processor));
    }

    processor.evaluate(results);
  }

  // --- object basics --------------------------------------------------------

}
