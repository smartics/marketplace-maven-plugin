/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.modules.mail.share;

import com.ecwid.maleorang.method.v3_0.lists.members.MemberInfo;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.HashSet;
import java.util.Set;

/**
 * Wrapper for information from a member of a list on Mailchimp.
 */
public class MailchimpListMember {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String email;

  private final String status;

  private final Set<String> tags;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private MailchimpListMember(final MemberInfo info, final Set<String> tags) {
    this.email = info.email_address;
    this.status = info.status;
    this.tags = tags;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static MailchimpListMember from(final MemberInfo info) {
    final String jsonString = info.toJson();
    final JsonParser parser = new JsonParser();
    final JsonObject member = (JsonObject) parser.parse(jsonString);
    final Set<String> tags = readTags(member);
    final MailchimpListMember instance = new MailchimpListMember(info, tags);
    return instance;
  }

  private static Set<String> readTags(final JsonObject member) {
    final Set<String> tags = new HashSet<>();

    final JsonElement tagCountElement = member.get("tags_count");
    if (tagCountElement != null) {
      final int tagCount = tagCountElement.getAsInt();
      if (tagCount > 0) {
        final JsonElement tagsElement = member.get("tags");
        final JsonArray tagsArray = tagsElement.getAsJsonArray();

        for (final JsonElement tagElement : tagsArray) {
          if (tagElement.isJsonObject()) {
            final JsonObject tag = (JsonObject) tagElement;
            final String name = tag.get("name").getAsString();
            tags.add(name);
          }
        }
      }
    }

    return tags;
  }

  // --- get&set --------------------------------------------------------------

  public String getEmail() {
    return email;
  }

  public String getStatus() {
    return status;
  }

  public Set<String> getTags() {
    return tags;
  }

  // --- business -------------------------------------------------------------

  public boolean hasTag(final String tag) {
    return tags.contains(tag);
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
