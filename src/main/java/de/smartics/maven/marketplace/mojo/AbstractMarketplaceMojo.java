/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo;

import static io.atlassian.fugue.Option.some;

import de.smartics.maven.marketplace.share.domain.AppException;
import de.smartics.maven.marketplace.tools.maven.SettingsPropertiesManger;
import de.smartics.maven.marketplace.tools.security.SettingsDecrypter;
import de.smartics.maven.marketplace.tools.types.StringUtilities;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MarketplaceClientFactory;
import com.atlassian.marketplace.client.MpacException.ServerError;
import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;

import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Profile;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.StringUtils;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import java.net.URI;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Nullable;

/**
 * Base implementation for all mojos accessing the Atlassian Marketplace server
 * in this project.
 *
 * @since 1.0
 */
public abstract class AbstractMarketplaceMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The default size of returned items per page.
   */
  private static final int DEFAULT_PAGE_SIZE = 50;

  /**
   * The key allows users to list relevant addon keys in their settings file.
   * This value is rendered in case no addon key is specified by the user.
   *
   * @see {@link #addonKey}
   */
  private static final String SETTINGS_ADDONKEYS_KEY =
      "marketplace.promotedAddonKeys";

  // --- members --------------------------------------------------------------

  /**
   * Helper to decrypt passwords from the settings.
   *
   * @since 1.0
   */
  @Component(role =
      org.sonatype.plexus.components.sec.dispatcher.SecDispatcher.class)
  private SecDispatcher securityDispatcher;

  /**
   * The location of the <code>settings-security.xml</code>.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${user.home}/.m2/settings-security.xml")
  private String settingsSecurityLocation;

  /**
   * The name of the user to authenticate to the remote server.
   * <p>
   * Set with '<tt>-Dmarketplace.username</tt>' or '<tt>-Dusername</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.username")
  private String username;

  /**
   * The password of the user to authenticate to the remote server.
   * <p>
   * Set with '<tt>-Dmarketplace.password</tt>' or '<tt>-Dpassword</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.password")
  private String password;

  /**
   * The identifier for the server to fetch credentials in case
   * <tt>username</tt> or <tt>password</tt> are not set explicitly. The
   * credentials are used to connect to the REST API of the remote server.
   * <p>
   * Defaults to '<tt>atlassian-marketplace</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.serverId</tt>' or '<tt>-DserverId</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.serverId")
  private String serverId;

  /**
   * The base server URL to locate services.
   * <p>
   * Set with '<tt>-Dmarketplace.serverUrl</tt>' or '<tt>-DserverUrl</tt>'.
   * </p>
   * <p>
   * Defaults to <tt>https://marketplace.atlassian.com</tt>.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.serverUrl")
  private String serverUrl;

  /**
   * The size of the page to query licenses. The process iterates over chunks of
   * this count. So the plugin will query as long as there are hits. But each
   * request will return at maximum this number of licenses.
   * <p>
   * Set with '<tt>-Dmarketplace.pageSize</tt>' or '<tt>-DpageSize</tt>'.
   * </p>
   * <p>
   * The value may be overridden by the Atlassian Marketplace service.
   * </p>
   * <p>
   * Defaults to max. 50 licenses per page of a single request.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.pageSize")
  private Integer pageSize;

  /**
   * The key that identifies the addon on the Atlassian Marketplace.
   * <p>
   * Set with '<tt>-Dmarketplace.addonKey</tt>' or '<tt>-DaddonKey</tt>'.
   * </p>
   * <p>
   * Use a settings property named <tt>marketplace.promotedAddonKeys</tt> to
   * list valid addon keys. The keys are separated by comma. The values may help
   * users to recall valid values for this parameter.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.addonKey")
  private String addonKey;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************


  public class BaseMarketplaceContext extends BaseContext {
    public BaseMarketplaceContext() {
    }

    /**
     * Translates the app key into a configured identifier.
     *
     * @param appKey the key of the app to translate.
     * @return the translation or the app key if no translation is configured.
     */
    public String translateAppKey(final String appKey) {
      return getSettingsPropertiesManager().getProperty(appKey, appKey);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getUsername() {
    if (StringUtils.isBlank(username)) {
      return getProperty("username");
    }
    return username;
  }

  public String getPassword() {
    if (StringUtils.isBlank(password)) {
      return getProperty("password");
    }
    return password;
  }

  public String getServerId() {
    if (StringUtils.isBlank(serverId)) {
      return getProperty("serverId", "atlassian-marketplace");
    }
    return serverId;
  }

  public String getServerUrl() {
    if (StringUtils.isBlank(serverUrl)) {
      return getProperty("serverUrl",
          MarketplaceClientFactory.DEFAULT_MARKETPLACE_URI.toASCIIString());
    }
    return serverUrl;
  }

  public int getPageSize() {
    if (pageSize == null || pageSize < 1) {
      final Integer envValue = getPropertyAsInteger("pageSize");
      if (envValue == null) {
        return DEFAULT_PAGE_SIZE;
      }
    }
    return pageSize;
  }

  public String getAddonKey() {
    if (StringUtils.isBlank(addonKey)) {
      return getProperty("addonKey");
    }
    return addonKey;
  }

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {
    checkArguments();
  }

  protected void checkArguments() {
    final boolean hasUsername = StringUtils.isNotBlank(getUsername());
    final boolean hasPassword = StringUtils.isNotBlank(getPassword());
    if (!(hasUsername && hasPassword)) {
      provideCredentialsFromServerConfig(hasUsername, hasPassword);
    }

    if (StringUtils.isBlank(getServerUrl())) {
      throw new AppException(
          "URL to the server of the Atlassian Marketplace must be set, but is" +
          " missing." +
          " Use -Dmarketplace.serverUrl to specifiy the URL the Atlassian " +
          "Marketplace.");
    }

    checkAddonKey();
  }

  protected ExtendedMarketplaceClient createClient(
      final BaseMarketplaceContext context) {
    final String serverUrl = getServerUrl();
    final String username = getUsername();
    final String password = getPassword();

    final URI baseUri = URI.create(serverUrl);
    if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
      context.logVerbose(
          "Authenticate to '" + serverUrl + "' as user '" + username +
          "' using " + (StringUtils.isNotBlank(password) ? "" : "no ") +
          "password.");
      final HttpConfiguration.Credentials credentials =
          new HttpConfiguration.Credentials(username, password);
      final HttpConfiguration configuration =
          HttpConfiguration.builder().credentials(some(credentials)).build();
      final MarketplaceClient client = MarketplaceClientFactory
          .createMarketplaceClient(baseUri, configuration);
      return new ExtendedMarketplaceClient(baseUri, client);
    } else {
      final MarketplaceClient client = MarketplaceClientFactory
          .createMarketplaceClient(URI.create(serverUrl));
      return new ExtendedMarketplaceClient(baseUri, client);
    }
  }

  private void provideCredentialsFromServerConfig(final boolean hasUsername,
      final boolean hasPassword) throws AppException {
    final String serverId = getServerId();
    final Server server = fetchServer(serverId);
    if (server != null) {
      if (!hasUsername) {
        username = server.getUsername();
      }
      if (!hasPassword) {
        final String plain = server.getPassword();
        try {
          final SettingsDecrypter settingsDecrypter = createSettingsDecrypter();
          password = settingsDecrypter.decrypt(plain);
        } catch (final SecDispatcherException e) {
          getLog().warn(
              "Cannot decrypt password for server '" + serverId + "': " +
              e.getMessage() +
              " -> Using plain password which will work if the password" +
              " is actually not encrypted. See" +
              " https://maven.apache.org/guides/mini/guide-encryption.html" +
              " for details.");
          password = plain;
        }
      }
    }
  }

  private void checkAddonKey() {
    final String addonKey = getAddonKey();
    if (StringUtils.isBlank(addonKey)) {
      final StringBuilder buffer = new StringBuilder(128);
      buffer.append(
          "Cannot connect to Atlassian Marketplace for license report without" +
          " the addon key. Use -DaddonKey to specify");
      final SettingsPropertiesManger settingsProperties =
          getSettingsPropertiesManager();
      final String configuredAddonKeys =
          settingsProperties.getProperty(SETTINGS_ADDONKEYS_KEY);
      if (StringUtils.isEmpty(configuredAddonKeys)) {
        buffer.append(" the key of your addon.");
      } else {
        buffer.append(" one of the following addon keys: ");
        for (final String configuredAddonKey : StringUtilities
            .splitByComma(configuredAddonKeys, true)) {
          buffer.append("\n   - ").append(configuredAddonKey);
        }
      }
      final String message = buffer.toString();
      throw new AppException(message);
    }
  }

  protected SettingsDecrypter createSettingsDecrypter() {
    return new SettingsDecrypter(securityDispatcher, settingsSecurityLocation);
  }

  @Nullable
  protected Server fetchServer(final String serverId) throws AppException {
    final Settings settings = mavenSession.getSettings();
    if (settings == null) {
      getLog().debug(
          "No settings provided to find server configuration '" + serverId +
          "'. Cannot provide credentials. See" +
          " https://maven.apache.org/settings.html for details.");
      return null;
    }
    final Server server = settings.getServer(serverId);
    if (server == null) {
      getLog().debug("No server configuration found for '" + serverId +
                     "'. Cannot provide credentials. See" +
                     " https://maven.apache.org/settings.html#Servers for " +
                     "details.");
    }
    return server;
  }

  protected void logRestCommunicationProblems(
      final BaseMarketplaceContext context, final String appKey,
      final Throwable e) {
    final String message = "Skipping '" + appKey +
                           "' because of communication problems talking to " +
                           "the Atlassian Marketplace server: ";
    logRestCommunicationProblems(context, appKey, message, e);
  }

  protected void logRestCommunicationProblems(
      final BaseMarketplaceContext context, final String appKey,
      final String message, final Throwable e) {
    final Throwable cause = e.getCause();
    if (cause instanceof ServerError) {
      final ServerError restCause = (ServerError) cause;
      final int statusCode = restCause.getStatus();
      final String serverUrl = getServerUrl();
      if (statusCode == 401 || statusCode == 403) {
        context.logError("HTTP " + statusCode + " from " + serverUrl +
                         "\n Cannot access resources on the Atlassian " +
                         "Marketplace due to " +
                         (statusCode == 401 ? "authentication" :
                          "authorization") + " problems.");
      } else {
        context.logError(
            "HTTP " + statusCode + " from " + serverUrl + "\n" + message +
            restCause.getMessage());
      }
    } else {
      context.logError("Skipping '" + appKey +
                       "' because of communication problems talking to the " +
                       "Atlassian Marketplace: " + e.getMessage());
    }
  }

  // --- object basics --------------------------------------------------------

}
