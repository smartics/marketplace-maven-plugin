/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.mail;

import static de.smartics.maven.marketplace.mojo.market.MarketplaceTransitionsMojo.DEFAULT_EVALUATION_REMINDER_PERIOD;
import static de.smartics.maven.marketplace.mojo.market.MarketplaceTransitionsMojo.DEFAULT_INCREMENT_PATTERN;
import static io.atlassian.fugue.Option.some;

import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.VendorSource;
import de.smartics.maven.marketplace.modules.mail.controller.MailchimpSync;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListClient;
import de.smartics.maven.marketplace.modules.market.controller.LicensePrinter;
import de.smartics.maven.marketplace.tools.types.DateIncrement;
import de.smartics.maven.marketplace.tools.types.TimeMachine;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.MpacException.ServerError;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.joda.time.LocalDate;

import java.util.Optional;

import io.atlassian.fugue.Option;

/**
 * Synchronize the customer information on the Atlassian Marketplace with the
 * customers on Mailchimp.
 * <p>
 * The mojo uses a Mailchimp API 3.0 REST client.
 * </p>
 *
 * @since 1.0
 */
public abstract class AbstractMailchimpSyncMojo extends AbstractMailchimpMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Defines the current date. Defaults to the current system date.
   * <p>
   * Specify the date in the ISO format <tt>yyyy-MM-dd</tt>.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.now</tt>' or '<tt>-Dnow</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.now")
  private String now;

  /**
   * The increment pattern to apply to the current date. Defaults to
   * <tt>-36h</tt>. This defines the start date relative to the event in time
   * defined as now.
   * <p>
   * Set with '<tt>-Dmarketplace.incrementPattern</tt>' or
   * '<tt>-DincrementPattern</tt>'.
   * </p>
   * <p>
   * Specify adjustments to an instance of time.
   * </p>
   * <p>
   * The following format specifies the positive or negative increment.
   * </p>
   *
   * <pre>
   * {+|-}?{amount}?{unit}
   * </pre>
   * <p>
   * Use the '-' sign to go back in time, no sign or '+' to go to a future
   * instance of time.
   * </p>
   * <p>
   * If no amount is specified, a value of <tt>1</tt> is the default.
   * </p>
   * <p>
   * The following units are defined:
   * </p>
   * <table>
   * <tr>
   * <th>Symbol</th>
   * <th>Description</th>
   * </tr>
   * <tr>
   * <td>m</td>
   * <td>Minutes</td>
   * </tr>
   * <tr>
   * <td>h</td>
   * <td>Hours</td>
   * </tr>
   * <tr>
   * <td>d</td>
   * <td>Days</td>
   * </tr>
   * <tr>
   * <td>w</td>
   * <td>Weeks</td>
   * </tr>
   * <tr>
   * <td>M</td>
   * <td>Months</td>
   * </tr>
   * <tr>
   * <td>y</td>
   * <td>Years</td>
   * </tr>
   * </table>
   * <p>
   * Examples:
   * </p>
   * <ul>
   * <li><tt>-1d</tt> - subtract one day</li>
   * <li><tt>+1d-10m</tt> - add one day and subtract 10 minutes</li>
   * <li><tt>d-10m</tt> - add one day and subtract 10 minutes</li>
   * </ul>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.incrementPattern")
  private String incrementPattern;

  /**
   * Defines the duration relative to the start date. If not set, all licenses
   * from the calculated start date to the current date.
   * <p>
   * See incrementPattern for valid values. The value should be a positive
   * value.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.endPeriod</tt>' or '<tt>-DendPeriod</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.endPeriod")
  private String endPeriod;

  /**
   * Defines the duration relative to the end of evaluation. The value defaults
   * to <tt>-4d</tt>.
   * <p>
   * See incrementPattern for valid values. The value should be a negative
   * value.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.evaluationReminderPeriod</tt>' or
   * '<tt>-DevaluationReminderPeriod</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.evaluationReminderPeriod")
  private String evaluationReminderPeriod;

  /**
   * Development flag that will be removed in the production version. We may
   * provide a dryRun option later. Currently we need this flag which is more
   * secure than dryRun since it requires to actively set it. Later this should
   * be the default behaviour.
   */
  @Parameter(property = "runWithMailChimp", defaultValue = "false")
  private boolean runWithMailChimp;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getNow() {
    if (StringUtils.isBlank(now)) {
      return getProperty("now");
    }
    return now;
  }

  public String getIncrementPattern() {
    if (StringUtils.isBlank(incrementPattern)) {
      String value = getProperty("incrementPattern");
      if (StringUtils.isBlank(value)) {
        value = DEFAULT_INCREMENT_PATTERN;
      }
      return value;
    }
    return incrementPattern;
  }

  public String getEndPeriod() {
    if (StringUtils.isBlank(endPeriod)) {
      return getProperty("endPeriod");
    }
    return endPeriod;
  }

  public String getEvaluationReminderPeriod() {
    if (StringUtils.isBlank(evaluationReminderPeriod)) {
      String value = getProperty("evaluationReminderPeriod");
      if (StringUtils.isBlank(value)) {
        value = DEFAULT_EVALUATION_REMINDER_PERIOD;
      }
      return value;
    }
    return evaluationReminderPeriod;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseMarketplaceContext context = new BaseMarketplaceContext();
    if (isSkip()) {
      context.logInfo("Skipping synchronizing since skip='true'.");
      return;
    }

    init();

    final String addonKey = getAddonKey();
    final TimeMachine time = TimeMachine.create(getNow());
    final Option<LocalDate> startTime = time.calcDate(getIncrementPattern());
    final Option<LocalDate> endTime = time.calcDate(getEndPeriod());
    final LicenseQuery licenseQuery = createQuery(startTime, endTime);
    final LicenseProcessor processor = createLicenseProcessor(context, time);
    try (final ExtendedMarketplaceClient client = createClient(context)) {
      final VendorSource vendorSource =
          createVendorSource(client, licenseQuery);
      vendorSource.processWith(processor);
    } catch (final ServerError e) {
      this.logRestCommunicationProblems(context, addonKey,
          "Communication with Atlassian Marketplace for app '" + addonKey +
          "' failed.", e);
      throw new MojoExecutionException(
          "Cannot connect to the Atlassian Marketplace: " + e.getMessage(), e);
    } catch (final MpacException e) {
      throw new MojoExecutionException(
          "Cannot create REST client to connect to the Atlassian Marketplace:" +
          " " + e.getMessage(), e);
    }
  }

  protected abstract VendorSource createVendorSource(
      ExtendedMarketplaceClient client, LicenseQuery licenseQuery);

  private LicenseProcessor createLicenseProcessor(
      final BaseMarketplaceContext context, final TimeMachine time) {
    if (runWithMailChimp) { // FIXME
      final DateIncrement beforeEvaluationEnd =
          DateIncrement.fromString(evaluationReminderPeriod);

      final String mailchimpApiKey = getMailchimpApiKey();
      final String mailChimpListId = getMailchimpListId();
      final MailchimpListClient client =
          new MailchimpListClient(mailchimpApiKey, mailChimpListId);
      return new MailchimpSync(context, client, time.getNowLocal(),
          beforeEvaluationEnd);
    } else {
      return new LicensePrinter();
    }
  }

  private LicenseQuery createQuery(final Option<LocalDate> startTime,
      final Option<LocalDate> endTime) {
    final LicenseQuery licenseQuery =
        LicenseQuery.builder().licenseType(typesFromString())
            .sortBy(some("startDate"))
            .bounds(QueryBounds.limit(Optional.of(getPageSize())))
            .startDate(startTime).endDate(endTime).status(licenseStatus())
            .build();
    return licenseQuery;
  }

  // --- object basics --------------------------------------------------------

}
