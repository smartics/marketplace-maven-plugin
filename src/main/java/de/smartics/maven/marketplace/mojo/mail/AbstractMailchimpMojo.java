/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.mail;

import static java.util.Collections.emptyList;

import de.smartics.maven.marketplace.client.api.LicenseStatusType;
import de.smartics.maven.marketplace.client.api.LicenseType;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo;
import de.smartics.maven.marketplace.share.domain.AppException;
import de.smartics.maven.marketplace.tools.security.SettingsDecrypter;
import de.smartics.maven.marketplace.tools.types.StringUtilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import java.util.ArrayList;
import java.util.List;

/**
 * Base implementation for use cases having to access the Atlassian Marketplace
 * API and the Mailchimp API.
 * <p>
 * The mojo uses a Mailchimp API 3.0 REST client.
 * </p>
 *
 * @since 1.0
 */
public abstract class AbstractMailchimpMojo extends AbstractMarketplaceMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The list of license types to query.
   * <p>
   * Set with '<tt>-Dmarketplace.licenseTypes</tt>' or
   * '<tt>-DlicenseTypes</tt>'.
   * </p>
   * <p>
   * The following types are supported:
   * </p>
   * <ul>
   * <li><tt>evaluation</tt> - evaluation licenses</li>
   * <li><tt>licensed</tt> - academic, commercial, community, or open source
   * licenses</li>
   * </ul>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.licenseTypes")
  private String licenseTypes;

  /**
   * The list of license status to query.
   * <p>
   * Set with '<tt>-Dmarketplace.licenseStatus</tt>' or
   * '<tt>-DlicenseStatus</tt>'.
   * </p>
   * <p>
   * The following status are supported:
   * </p>
   * <ul>
   * <li><tt>evaluation</tt> - evaluation licenses</li>
   * <li><tt>licensed</tt> - academic, commercial, community, or open source
   * licenses</li>
   * </ul>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.licenseStatus")
  private String licenseStatus;

  /**
   * The key to authenticate to the Mailchimp server.
   * <p>
   * Set with '<tt>-Dmarketplace.mailchimpApiKey</tt>' or
   * '<tt>-DmailchimpApiKey</tt>'.
   * </p>
   * <p>
   * In case you want to set an encrypted API key, use the
   * <tt>mailchimpServerId</tt> and specify a <tt>server</tt> element in your
   * <tt>settings.xml</tt>.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.mailchimpApiKey")
  private String mailchimpApiKey;

  /**
   * The identifier for the Mailchimp server to fetch credentials in case
   * <tt>mailchimpApiKey</tt> is not set explicitly. The credentials are used to
   * connect to the REST API of the Mailchimp server.
   * <p>
   * Defaults to '<tt>mailchimp</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.mailchimpServerId</tt>' or
   * '<tt>-DmailchimpServerId</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.mailchimpServerId")
  private String mailchimpServerId;

  /**
   * The identifier of the list or audience on your Mailchimp account.
   * <p>
   * In case you have the free plan, there is only one audience.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.mailchimpListId</tt>' or
   * '<tt>-DmailchimpListId</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.mailchimpListId")
  private String mailchimpListId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getLicenseTypes() {
    if (StringUtils.isBlank(licenseTypes)) {
      return getProperty("licenseTypes");
    }
    return licenseTypes;
  }

  public String getLicenseStatus() {
    if (StringUtils.isBlank(licenseStatus)) {
      return getProperty("licenseStatus");
    }
    return licenseStatus;
  }

  public String getMailchimpApiKey() {
    if (StringUtils.isBlank(mailchimpApiKey)) {
      return getProperty("mailchimpApiKey");
    }
    return mailchimpApiKey;
  }

  public String getMailchimpListId() {
    if (StringUtils.isBlank(mailchimpListId)) {
      return getProperty("mailchimpListId");
    }
    return mailchimpListId;
  }

  public String getMailchimpServerId() {
    if (StringUtils.isBlank(mailchimpServerId)) {
      return getProperty("mailchimpServerId", "mailchimp");
    }
    return mailchimpServerId;
  }

  // --- business -------------------------------------------------------------

  @Override
  protected void checkArguments() throws AppException {
    super.checkArguments();

    checkMailchimpApiKey();
  }

  private void checkMailchimpApiKey() {
    final boolean hasApiKey = StringUtils.isNotBlank(getMailchimpApiKey());
    if (!hasApiKey) {
      final String serverId = getMailchimpServerId();
      final Server server = fetchServer(serverId);
      if (server != null) {
        final String plain = server.getPassword();
        try {
          final SettingsDecrypter settingsDecrypter = createSettingsDecrypter();
          mailchimpApiKey = settingsDecrypter.decrypt(plain);
        } catch (final SecDispatcherException e) {
          getLog().warn("Cannot decrypt password for server '" + serverId
              + "': " + e.getMessage()
              + " -> Using plain password which will work if the password"
              + " is actually not encrypted. See"
              + " https://maven.apache.org/guides/mini/guide-encryption.html"
              + " for details.");
          mailchimpApiKey = plain;
        }
      } else {
        throw new AppException(
            "No API key provided to access the REST API of the Mailchimp server.");
      }
    }
  }

  protected Iterable<LicenseStatusType> licenseStatus() {
    final String status = getLicenseStatus();
    final List<LicenseStatusType> list = new ArrayList<>(3);
    if (StringUtils.isNotBlank(status)) {
      for (final String value : StringUtilities.splitByComma(status, true)) {
        list.add(LicenseStatusType.fromString(value));
      }
      return list;
    }
    return emptyList();
  }

  protected Iterable<LicenseType> typesFromString() {
    final String licenseTypes = getLicenseTypes();
    if (StringUtils.isNotBlank(licenseTypes)) {
      switch (licenseTypes) {
        case "evaluation":
          return LicenseType.evaluationTypes();
        case "licensed":
          return LicenseType.licensedTypes();
        default:
          throw new IllegalArgumentException("License types '" + licenseTypes
              + "' unknown. Please specified a valid license type"
              + " (active, evalutation, expired, licensed) or leave blank for no constraints.");
      }
    }

    return emptyList();
  }

  // --- object basics --------------------------------------------------------

}
