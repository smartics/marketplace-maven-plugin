/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.mail;

import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.VendorSource;
import de.smartics.maven.marketplace.modules.market.controller.LicensesForThisVendor;

import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Synchronize the customer information on the Atlassian Marketplace with the
 * customers on Mailchimp.
 * <p>
 * The mojo uses a Mailchimp API 3.0 REST client.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "mailchimp-synchronize", threadSafe = true,
    requiresProject = false, defaultPhase = LifecyclePhase.NONE)
public class MailchimpSyncMojo extends AbstractMailchimpSyncMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected VendorSource createVendorSource(
      final ExtendedMarketplaceClient client, final LicenseQuery licenseQuery) {
    return LicensesForThisVendor.create(client, licenseQuery);
  }

  // --- object basics --------------------------------------------------------

}
