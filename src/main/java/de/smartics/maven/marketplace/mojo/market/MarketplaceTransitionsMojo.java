/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.market;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.MpacException.ServerError;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;
import de.smartics.maven.marketplace.client.api.LicenseIterator.LicenseProcessor;
import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.LicenseStatusType;
import de.smartics.maven.marketplace.modules.market.controller.LicensesForThisVendor;
import de.smartics.maven.marketplace.modules.market.controller.TransitionRules;
import de.smartics.maven.marketplace.mojo.AbstractMarketplaceMojo;
import de.smartics.maven.marketplace.share.domain.AppException;
import de.smartics.maven.marketplace.tools.types.DateIncrement;
import de.smartics.maven.marketplace.tools.types.TimeMachine;
import io.atlassian.fugue.Option;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.joda.time.LocalDate;

import java.util.Optional;

import static io.atlassian.fugue.Option.some;

/**
 * Checks status information for customers on the Atlassian Marketplace and
 * calculates the transition.
 * <p>
 * <code>mvn marketplace:transitions -Dverbose</code>
 * </p>
 * <p>
 * Prints information about changes applied to the user base in the last period
 * defined by the <code>incrementPattern</code>.
 * </p>
 * <p>
 * This tool basically allows to set a start date relative to now (which in turn
 * may also be defined to be a moment in the past). Relative to this start date,
 * the end date is set. If the end date is not specified, it defaults to now.
 * </p>
 * <p>
 * The <code>evaluationReminderPeriod</code> is relative to the end date. if the
 * current date is in this interval, a reminder may be send to the licensee to
 * signal the end of evaluation period.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "transitions", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class MarketplaceTransitionsMojo extends AbstractMarketplaceMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The default pattern to use to increment the start date.
   */
  public static final String DEFAULT_INCREMENT_PATTERN = "-36h";

  /**
   * The default pattern to use to increment the end date for the start of the
   * evaluation reminder period.
   */
  public static final String DEFAULT_EVALUATION_REMINDER_PERIOD = "-4d";

  // --- members --------------------------------------------------------------

  /**
   * Defines the current date. Defaults to the current system date.
   * <p>
   * Specify the date in the ISO format <tt>yyyy-MM-dd</tt>.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.now</tt>' or '<tt>-Dnow</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.now")
  private String now;

  /**
   * The increment pattern to apply to the current date. Defaults to
   * <tt>-36h</tt>. This defines the start date relative to the event in time
   * defined as now.
   * <p>
   * Set with '<tt>-Dmarketplace.incrementPattern</tt>' or
   * '<tt>-DincrementPattern</tt>'.
   * </p>
   * <p>
   * Specify adjustments to an instance of time.
   * </p>
   * <p>
   * The following format specifies the positive or negative increment.
   * </p>
   *
   * <pre>
   * {+|-}?{amount}?{unit}
   * </pre>
   * <p>
   * Use the '-' sign to go back in time, no sign or '+' to go to a future
   * instance of time.
   * </p>
   * <p>
   * If no amount is specified, a value of <tt>1</tt> is the default.
   * </p>
   * <p>
   * The following units are defined:
   * </p>
   * <table>
   * <tr>
   * <th>Symbol</th>
   * <th>Description</th>
   * </tr>
   * <tr>
   * <td>m</td>
   * <td>Minutes</td>
   * </tr>
   * <tr>
   * <td>h</td>
   * <td>Hours</td>
   * </tr>
   * <tr>
   * <td>d</td>
   * <td>Days</td>
   * </tr>
   * <tr>
   * <td>w</td>
   * <td>Weeks</td>
   * </tr>
   * <tr>
   * <td>M</td>
   * <td>Months</td>
   * </tr>
   * <tr>
   * <td>y</td>
   * <td>Years</td>
   * </tr>
   * </table>
   * <p>
   * Examples:
   * </p>
   * <ul>
   * <li><tt>-1d</tt> - subtract one day</li>
   * <li><tt>+1d-10m</tt> - add one day and subtract 10 minutes</li>
   * <li><tt>d-10m</tt> - add one day and subtract 10 minutes</li>
   * </ul>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.incrementPattern")
  private String incrementPattern;

  /**
   * Defines the duration relative to the start date. If not set, all licenses
   * from the calculated start date to the current date.
   * <p>
   * See incrementPattern for valid values. The value should be a positive
   * value.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.endPeriod</tt>' or '<tt>-DendPeriod</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.endPeriod")
  private String endPeriod;

  /**
   * Defines the duration relative to the end of evaluation. Defaults to
   * <tt>-4d</tt>.
   * <p>
   * See incrementPattern for valid values. The value should be a negative
   * value.
   * </p>
   * <p>
   * Set with '<tt>-Dmarketplace.evaluationReminderPeriod</tt>' or
   * '<tt>-DevaluationReminderPeriod</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.evaluationReminderPeriod")
  private String evaluationReminderPeriod;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getNow() {
    if (StringUtils.isBlank(now)) {
      return getProperty("now");
    }
    return now;
  }

  public String getIncrementPattern() {
    if (StringUtils.isBlank(incrementPattern)) {
      String value = getProperty("incrementPattern");
      if (StringUtils.isBlank(value)) {
        value = DEFAULT_INCREMENT_PATTERN;
      }
      return value;
    }
    return incrementPattern;
  }

  public String getEndPeriod() {
    if (StringUtils.isBlank(endPeriod)) {
      return getProperty("endPeriod");
    }
    return endPeriod;
  }

  public String getEvaluationReminderPeriod() {
    if (StringUtils.isBlank(evaluationReminderPeriod)) {
      String value = getProperty("evaluationReminderPeriod");
      if (StringUtils.isBlank(value)) {
        value = DEFAULT_EVALUATION_REMINDER_PERIOD;
      }
      return value;
    }
    return evaluationReminderPeriod;
  }

  // --- business -------------------------------------------------------------

  @Override
  protected void checkArguments() throws AppException {
    super.checkArguments();
  }

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseMarketplaceContext context = new BaseMarketplaceContext();
    if (isSkip()) {
      context.logInfo("Skipping transition check since skip='true'.");
      return;
    }

    init();

    final TimeMachine time = TimeMachine.create(getNow());
    final Option<LocalDate> startTime = time.calcDate(getIncrementPattern());
    final Option<LocalDate> endTime = time.calcDate(getEndPeriod());
    final DateIncrement beforeEvaluationEnd =
        DateIncrement.fromString(getEvaluationReminderPeriod());

    if (context.isVerbose()) {
      context.logInfo("Setting today to " + time.getNowLocal()
          + " with increment " + getIncrementPattern() + ".");
    }

    final String addonKey = getAddonKey();
    final LicenseQuery licenseQuery = createQuery(startTime, endTime);
    final LicenseProcessor processor = TransitionRules.create(context,
        time.getNowLocal(), beforeEvaluationEnd);
    try (final ExtendedMarketplaceClient client = createClient(context)) {
      LicensesForThisVendor.execute(client, licenseQuery, processor);
    } catch (final ServerError e) {
      this.logRestCommunicationProblems(context, addonKey,
          "Communication with Atlassian Marketplace for app '" + addonKey
              + "' failed.",
          e);
      throw new MojoExecutionException(
          "Cannot connect to the Atlassian Marketplace: " + e.getMessage(), e);
    } catch (final MpacException e) {
      throw new MojoExecutionException(
          "Cannot create REST client to connect to the Atlassian Marketplace: "
              + e.getMessage(),
          e);
    }
  }

  private LicenseQuery createQuery(final Option<LocalDate> startTime,
      final Option<LocalDate> endTime) {
    final LicenseQuery licenseQuery = LicenseQuery.builder()
        .sortBy(some("startDate"))
        .bounds(QueryBounds.limit(Optional.of(getPageSize())))
        .startDate(startTime)
        .endDate(endTime)
        .status(Option.some(LicenseStatusType.ACTIVE))
        .build();
    return licenseQuery;
  }

  // --- object basics --------------------------------------------------------

}
