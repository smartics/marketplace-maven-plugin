/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo;

import de.smartics.maven.marketplace.share.domain.AppException;
import de.smartics.maven.marketplace.share.domain.ExecutionContext;
import de.smartics.maven.marketplace.tools.maven.SettingsPropertiesManger;
import de.smartics.maven.marketplace.tools.maven.UserPropertiesManager;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;

/**
 * Base implementation for all mojos in this project.
 *
 * @since 1.0
 */
public abstract class AbstractBaseMojo extends AbstractMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Generic value to tell to skip the generation of a value. This is used to
   * prevent to calculate the default value from the root issue or any other
   * resource.
   */
  protected static final String SMARTICS_JIRA_SKIP_VALUE = "marketplace.skip";

  // --- members --------------------------------------------------------------

  /**
   * Project for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${project}", readonly = true)
  protected MavenProject mavenProject;

  /**
   * Session for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${session}", readonly = true)
  protected MavenSession mavenSession;

  /**
   * A simple flag to skip the deployment process.
   * <p>
   * Only support to be set with <tt>-Dmarketplace.skip</tt>.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.skip", defaultValue = "false")
  private boolean skip;

  /**
   * A simple flag to log verbosely.
   * <p>
   * Set with '<tt>-Dmarketplace.verbose</tt>' or '<tt>-Dverbose</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.verbose", defaultValue = "false")
  private boolean verbose;

  /**
   * A simple flag to populate the source, but without actually deploying to the
   * target.
   * <p>
   * Set with '<tt>-Dmarketplace.dryRun</tt>' or '<tt>-DdryRun</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.dryRun", defaultValue = "false")
  private boolean dryRun;

  /**
   * Provides access to additional properties set on the commandline.
   */
  private UserPropertiesManager userPropertiesManager;

  /**
   * Provides access to additional properties set in the user's settings file.
   */
  private SettingsPropertiesManger settingsPropertiesManger;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public class BaseContext implements ExecutionContext {

    private final Log log;

    protected BaseContext() {
      this.log = getLog();
    }

    @Override
    public void logInfo(final String message) {
      log.info(message);
    }

    @Override
    public void logDebug(final String message) {
      log.debug(message);
    }

    @Override
    public boolean isDebug() {
      return log.isDebugEnabled();
    }

    public void logWarn(final String message) {
      log.warn(message);
    }

    public void logError(final String message) {
      log.error(message);
    }

    public void logError(final String message, final Throwable e) {
      log.error(message, e);
    }

    @Override
    public void logVerbose(final String message) {
      if (isVerbose()) {
        log.info(message);
      }
    }

    @Override
    public void logVerbose(final Object appSource) {
      if (isVerbose()) {
        log.info(appSource.toString());
      }
    }

    @Override
    public boolean isVerbose() {
      return AbstractBaseMojo.this.isVerbose();
    }

    @Override
    public boolean isDryRun() {
      return AbstractBaseMojo.this.isDryRun();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the version of the Maven project.
   *
   * @return the version or <code>null</code> if the version is not set or no
   *         project is found.
   */
  protected String getProjectVersion() {
    if (mavenProject != null) {
      final String version = mavenProject.getVersion();
      if (StringUtils.isNotBlank(version)) {
        return version;
      }
    }

    return null;
  }

  public UserPropertiesManager getUserPropertiesManager() {
    if (userPropertiesManager == null) {
      userPropertiesManager = new UserPropertiesManager(mavenSession);
    }

    return userPropertiesManager;
  }

  public SettingsPropertiesManger getSettingsPropertiesManager() {
    if (settingsPropertiesManger == null) {
      settingsPropertiesManger = new SettingsPropertiesManger(mavenSession);
    }

    return settingsPropertiesManger;
  }


  protected boolean isSkip() {
    return skip;
  }

  private boolean isVerbose() {
    return verbose
        || getUserPropertiesManager().getPropertyAsBoolean("verbose");
  }

  private boolean isDryRun() {
    return dryRun || getUserPropertiesManager().getPropertyAsBoolean("dryRun");
  }

  protected boolean hasPom() {
    return mavenProject != null && !mavenProject.getId()
        .startsWith("org.apache.maven:standalone-pom:pom:");
  }

  protected String getProperty(final String name) {
    return getProperty(name, null);
  }

  protected String getProperty(final String name, final String defaultValue) {
    final String value =
        getUserPropertiesManager().getProperty("marketplace", name);
    if (StringUtils.isNotBlank(value)) {
      return value;
    }
    final String globalValue = getUserPropertiesManager().getProperty(name);
    if (StringUtils.isNotBlank(globalValue)) {
      return globalValue;
    }

    return defaultValue;
  }

  protected Integer getPropertyAsInteger(final String name) {
    return getPropertyAsInteger(name, null);
  }

  protected Integer getPropertyAsInteger(final String name,
      final Integer defaultValue) {
    final String value =
        getUserPropertiesManager().getProperty("marketplace", name);
    if (StringUtils.isNotBlank(value)) {
      try {
        return Integer.parseInt(value);
      } catch (final Exception e) {
        // continue with global
      }
    }
    final String globalValue = getUserPropertiesManager().getProperty(name);
    if (StringUtils.isNotBlank(globalValue)) {
      try {
        return Integer.parseInt(globalValue);
      } catch (final Exception e) {
        // continue with default
      }
    }

    return defaultValue;
  }

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {}

  // --- object basics --------------------------------------------------------

}
