/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.mail;

import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.VendorSource;
import de.smartics.maven.marketplace.modules.market.controller.LicensesForVendorByStream;
import de.smartics.maven.marketplace.share.domain.AppException;

import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Imports customer information from a JSON file into Mailchimp.
 * <p>
 * This mojo allows to initialize the Mailchimp configuration without access to
 * the Marketplace data. Useful also to test use cases with Mailchimp with test
 * data.
 * </p>
 * <p>
 * The mojo uses a Mailchimp API 3.0 REST client.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "mailchimp-import", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class MailchimpImportMojo extends AbstractMailchimpSyncMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Reference to a JSON file with licenses.
   */
  @Parameter(required = true, property = "licenses")
  private File licenses;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected VendorSource createVendorSource(
      final ExtendedMarketplaceClient client, final LicenseQuery licenseQuery) {
    try (final InputStream reader =
        new BufferedInputStream(new FileInputStream(licenses))) {
      return LicensesForVendorByStream.create(reader);
    } catch (final Exception e) {
      throw new AppException("Cannot read licenses file "
          + licenses.getAbsolutePath() + ":" + e.getMessage());
    }
  }

  @Override
  protected ExtendedMarketplaceClient createClient(
      final BaseMarketplaceContext context) {
    // Make sure to not connect to the Marketplace server since the license data
    // is provided by a file.
    return null;
  }

  // --- object basics --------------------------------------------------------

}
