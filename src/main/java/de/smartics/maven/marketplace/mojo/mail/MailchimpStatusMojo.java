/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.mojo.mail;

import de.smartics.maven.marketplace.modules.mail.services.GetListMember;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListClient;
import de.smartics.maven.marketplace.modules.mail.share.MailchimpListMember;
import de.smartics.maven.marketplace.share.domain.AppException;

import com.ecwid.maleorang.MailchimpException;

import io.atlassian.fugue.Option;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Checks status information for customers on the Atlassian Marketplace with
 * those managed on Mailchimp.
 * <p>
 * The mojo uses a Mailchimp API 3.0 REST client.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "mailchimp-status", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class MailchimpStatusMojo extends AbstractMailchimpMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The key that identifies the member of a list on Mailchimp.
   * <p>
   * Set with '<tt>-Dmarketplace.memberEmail</tt>' or '<tt>-DmemberEmail</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "marketplace.memberEmail")
  private String memberEmail;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getMemberEmail() {
    if (StringUtils.isBlank(memberEmail)) {
      return getProperty("memberEmail");
    }
    return memberEmail;
  }


  // --- business -------------------------------------------------------------

  @Override
  protected void checkArguments() throws AppException {
    super.checkArguments();

    if (StringUtils.isBlank(getMemberEmail())) {
      throw new AppException(
          "The identifying email adress of the member to fetch a status report"
              + " must be set. Please specifiy one with -DmemberEmail");
    }
  }

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseMarketplaceContext context = new BaseMarketplaceContext();
    if (isSkip()) {
      context.logInfo("Skipping status since skip='true'.");
      return;
    }

    init();

    final String mailchimpApiKey = getMailchimpApiKey();
    final String mailChimpListId = getMailchimpListId();
    final MailchimpListClient client =
        new MailchimpListClient(mailchimpApiKey, mailChimpListId);
    try {
      final GetListMember service = new GetListMember(client);
      final Option<MailchimpListMember> member = service.execute(getMemberEmail());
      getLog().info(member.isDefined()?member.getOrNull().toString():"null");
    }
/*
    catch (final MailchimpException e) {
      if (404 == e.code) {
        getLog().info("No member found by specified email address.");
      } else {
        throw new MojoExecutionException(
            "Cannot read member from Mailchimp list.", e);
      }
    }
*/
    catch (final Exception e) {
      throw new MojoExecutionException(
          "Cannot read member from Mailchimp list.", e);
    }
  }

  // --- object basics --------------------------------------------------------

}
