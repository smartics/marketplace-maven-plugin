/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.client.api;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;
import static java.util.Collections.emptyList;

import com.atlassian.marketplace.client.api.HostingType;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.api.QueryBuilderProperties;
import com.atlassian.marketplace.client.api.QueryProperties;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import org.joda.time.LocalDate;

import io.atlassian.fugue.Option;

/**
 *
 */
public final class LicenseQuery implements QueryProperties.Bounds {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final LicenseQuery DEFAULT_QUERY = builder().build();

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------


  private final Iterable<String> addon;
  private final Option<LocalDate> startDate;
  private final Option<LocalDate> endDate;
  private final Option<String> text;
  private final Iterable<Integer> tier;
  private final Option<String> dateType;
  private final Iterable<LicenseType> licenseType;
  private final Iterable<PartnerType> partnerTypes;
  private final Iterable<HostingType> hosting;
  private final Iterable<LicenseStatusType> status;
  private final Option<LocalDate> lastUpdated;
  private final Option<String> sortBy;
  private final Option<String> order;
  private final QueryBounds bounds;

  /**
   * Returns a new {@link Builder} for constructing a VendorQuery.
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Returns a VendorQuery with default criteria.
   */
  public static LicenseQuery any() {
    return DEFAULT_QUERY;
  }

  /**
   * Returns a new {@link Builder} for constructing a VendorQuery based on an
   * existing VendorQuery.
   */
  public static Builder builder(final LicenseQuery query) {
    return builder().addon(query.getAddon())
        .startDate(query.getStartDate())
        .endDate(query.getEndDate())
        .text(query.getText())
        .tier(query.getTier())
        .dateType(query.getDateType())
        .licenseType(query.getLicenseType())
        .partnerTypes(query.getPartnerTypes())
        .hosting(query.getHosting())
        .status(query.getStatus())
        .lastUpdated(query.getLastUpdated())
        .sortBy(query.getSortBy())
        .order(query.getOrder())
        .bounds(query.getBounds());
  }

  private LicenseQuery(final Builder builder) {
    addon = copyOf(builder.addon);
    startDate = builder.startDate;
    endDate = builder.endDate;
    text = builder.text;
    tier = copyOf(builder.tier);
    dateType = builder.dateType;
    licenseType = copyOf(builder.licenseType);
    partnerTypes = copyOf(builder.partnerTypes);
    hosting = builder.hosting;
    status = copyOf(builder.status);
    lastUpdated = builder.lastUpdated;
    sortBy = builder.sortBy;
    order = builder.order;
    bounds = builder.bounds;
  }

  @Override
  public QueryBounds getBounds() {
    return bounds;
  }

  public Iterable<String> getAddon() {
    return addon;
  }

  public Option<LocalDate> getStartDate() {
    return startDate;
  }

  public Option<LocalDate> getEndDate() {
    return endDate;
  }

  public Option<String> getText() {
    return text;
  }

  public Iterable<Integer> getTier() {
    return tier;
  }

  public Option<String> getDateType() {
    return dateType;
  }

  public Iterable<LicenseType> getLicenseType() {
    return licenseType;
  }

  public Iterable<PartnerType> getPartnerTypes() {
    return partnerTypes;
  }

  public Iterable<HostingType> getHosting() {
    return hosting;
  }

  public Iterable<LicenseStatusType> getStatus() {
    return status;
  }

  public Option<LocalDate> getLastUpdated() {
    return lastUpdated;
  }

  public Option<String> getSortBy() {
    return sortBy;
  }

  public Option<String> getOrder() {
    return order;
  }

  @Override
  public String toString() {
    return describeParams("LicenseQuery", describeValues("addon", addon),
        describeValues("startDate", startDate),
        describeValues("endDate", endDate), describeValues("text", text),
        describeValues("tier", tier), describeValues("dateType", dateType),
        describeValues("licenseType", licenseType),
        describeValues("partnerTypes", partnerTypes),
        describeValues("hosting", hosting), describeValues("status", status),
        describeValues("lastUpdated", lastUpdated),
        describeValues("sortBy", sortBy), describeValues("order", order),
        bounds.describe());
  }

  @Override
  public boolean equals(final Object other) {
    return (other instanceof LicenseQuery) &&
           toString().equals(other.toString());
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  /**
   * Builder class for {@link LicenseQuery}. Use {@link LicenseQuery#builder()}
   * to create an instance.
   */
  public static class Builder
      implements QueryBuilderProperties.Bounds<Builder> {
    private QueryBounds bounds = QueryBounds.defaultBounds();
    private Iterable<String> addon = emptyList();
    private Option<LocalDate> startDate = none();
    private Option<LocalDate> endDate = none();
    private Option<String> text = none();
    private Iterable<Integer> tier = emptyList();
    private Option<String> dateType = none();
    private Iterable<LicenseType> licenseType = emptyList();
    private Iterable<PartnerType> partnerTypes = emptyList();
    private Iterable<HostingType> hosting = emptyList();
    private Iterable<LicenseStatusType> status = emptyList();
    private Option<LocalDate> lastUpdated = none();
    private Option<String> sortBy = none();
    private Option<String> order = none();

    /**
     * Returns an immutable {@link LicenseQuery} based on the current builder
     * properties.
     */
    public LicenseQuery build() {
      return new LicenseQuery(this);
    }

    public LicenseQuery.Builder addon(final Iterable<String> addon) {
      this.addon = copyOf(addon);
      return this;
    }

    public LicenseQuery.Builder startDate(final Option<LocalDate> startDate) {
      this.startDate = checkNotNull(startDate);
      return this;
    }

    public LicenseQuery.Builder endDate(final Option<LocalDate> endDate) {
      this.endDate = checkNotNull(endDate);
      return this;
    }

    public LicenseQuery.Builder text(final Option<String> text) {
      this.text = checkNotNull(text);
      return this;
    }

    public LicenseQuery.Builder tier(final Iterable<Integer> tier) {
      this.tier = copyOf(tier);
      return this;
    }

    public LicenseQuery.Builder dateType(final Option<String> dateType) {
      this.dateType = checkNotNull(dateType);
      return this;
    }

    public LicenseQuery.Builder licenseType(
        final Iterable<LicenseType> licenseType) {
      this.licenseType = copyOf(licenseType);
      return this;
    }

    public LicenseQuery.Builder partnerTypes(
        final Iterable<PartnerType> partnerTypes) {
      this.partnerTypes = copyOf(partnerTypes);
      return this;
    }

    public LicenseQuery.Builder hosting(final Iterable<HostingType> hosting) {
      this.hosting = copyOf(hosting);
      return this;
    }

    public LicenseQuery.Builder status(
        final Iterable<LicenseStatusType> status) {
      this.status = copyOf(status);
      return this;
    }

    public LicenseQuery.Builder lastUpdated(
        final Option<LocalDate> lastUpdated) {
      this.lastUpdated = checkNotNull(lastUpdated);
      return this;
    }

    public LicenseQuery.Builder sortBy(final Option<String> sortBy) {
      this.sortBy = checkNotNull(sortBy);
      return this;
    }

    public LicenseQuery.Builder order(final Option<String> ord) {
      this.order = checkNotNull(order);
      return this;
    }

    @Override
    public Builder bounds(final QueryBounds bounds) {
      this.bounds = checkNotNull(bounds);
      return this;
    }
  }

  private static String describeParams(final String className,
      final Iterable<?>... paramLists) {
    return className + "(" + Joiner.on(", ").join(concat(paramLists)) + ")";
  }

  private static Iterable<String> describeValues(final String name,
      final Iterable<?> values) {
    if (!Iterables.isEmpty(values)) {
      return some(name + "(" + Joiner.on(",").join(values) + ")");
    }
    return none();
  }

}
