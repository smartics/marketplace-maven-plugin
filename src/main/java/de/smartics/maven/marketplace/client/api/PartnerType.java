/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.client.api;

import com.atlassian.marketplace.client.api.EnumWithKey;

public enum PartnerType implements EnumWithKey {
  // ***************************** Enumeration ******************************

  DIRECT("direct"),

  EXPERT("expert"),

  RESELLER("reseller");

  // ******************************** Fields ********************************

  // --- constants ----------------------------------------------------------

  // --- members ------------------------------------------------------------

  private final String key;

  // ***************************** Constructors *****************************

  PartnerType(final String key) {
    this.key = key;
  }

  // ******************************** Methods *******************************

  // --- init ---------------------------------------------------------------

  // --- get&set ------------------------------------------------------------

  @Override
  public String getKey() {
    return key;
  }

  // --- business -----------------------------------------------------------

  // --- object basics ------------------------------------------------------

  @Override
  public String toString() {
    return key;
  }
}
