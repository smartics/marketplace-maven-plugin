/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.client.api;

import de.smartics.maven.marketplace.client.model.License;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.impl.ExtendedMarketplaceClient;
import com.atlassian.marketplace.client.model.VendorSummary;
import com.google.common.collect.Lists;

import java.util.List;

import io.atlassian.fugue.Either;

/**
 *
 */
public class LicenseIterator {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ExtendedMarketplaceClient client;

  private final VendorSummary vendor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public LicenseIterator(final ExtendedMarketplaceClient client,
      final VendorSummary vendor) {
    this.client = client;
    this.vendor = vendor;
  }

  // ****************************** Inner Classes *****************************

  public interface LicenseProcessor {
    Either<Exception, String> process(License license) throws RuntimeException;

    default void evaluate(final List<Either<Exception, String>> results) {
      for (final Either<Exception, String> result : results) {
        System.out.println(
            result.isLeft() ? result.left().get() : result.right().get());
      }
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------


  public List<Either<Exception, String>> execute(
      final LicenseQuery licenseQuery, final LicenseProcessor processor)
      throws MpacException {
    final List<Either<Exception, String>> results = Lists.newArrayList();
    Page<License> licensePage = client.licenses(vendor).find(licenseQuery);
    process(processor, results, licensePage);
    while (licensePage.safeGetNext().isPresent()) {
      final PageReference<License> next = licensePage.safeGetNext().get();
      licensePage = client.getMore(next);
      process(processor, results, licensePage);
    }

    return results;
  }

  private void process(final LicenseProcessor processor,
      final List<Either<Exception, String>> results,
      final Page<License> licensePage) {
    for (final License license : licensePage) {
      results.add(processor.process(license));
    }
  }

  // --- object basics --------------------------------------------------------

}
