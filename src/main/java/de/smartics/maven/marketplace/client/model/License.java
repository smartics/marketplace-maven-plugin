/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.client.model;

import de.smartics.maven.marketplace.tools.types.PrettyPrinter;
import de.smartics.maven.marketplace.tools.types.PrettyPrinter.Prettifiable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDate;

import io.atlassian.fugue.Option;

/**
 * License information for an add-on on the Atlassian Marketplace.
 */
public final class License implements Prettifiable {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private String addonLicenseId;
  private Option<String> hostLicenseId;
  private String licenseId;
  private String addonKey;
  private String addonName;
  private String hosting;
  private LocalDate lastUpdated;
  private String licenseType;
  private LocalDate maintenanceStartDate;
  private LocalDate maintenanceEndDate;
  private String status;
  private String tier;
  private ContactDetails contactDetails;
  private Option<PartnerDetails> partnerDetails;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public static final class Contact {
    private Option<String> email;
    private Option<String> name;
    private Option<String> phone;
    private Option<String> address1;
    private Option<String> address2;
    private Option<String> city;
    private Option<String> state;
    private Option<String> postcode;

    public Option<String> getEmail() {
      return email;
    }

    public Option<String> getName() {
      return name;
    }

    public Option<String> getPhone() {
      return phone;
    }

    public Option<String> getAddress1() {
      return address1;
    }

    public Option<String> getAddress2() {
      return address2;
    }

    public Option<String> getCity() {
      return city;
    }

    public Option<String> getState() {
      return state;
    }

    public Option<String> getPostcode() {
      return postcode;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.MULTI_LINE_STYLE);
    }
  }

  public static final class ContactDetails {
    private String company;
    private String country;
    private String region;
    private Option<Contact> technicalContact;
    private Option<Contact> billingContact;

    public String getCompany() {
      return company;
    }

    public String getCountry() {
      return country;
    }

    public String getRegion() {
      return region;
    }

    public Option<Contact> getTechnicalContact() {
      return technicalContact;
    }

    public Option<Contact> getBillingContact() {
      return billingContact;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.MULTI_LINE_STYLE);
    }
  }

  public static final class PartnerDetails {
    private String partnerName;
    private String partnerType;
    private Option<Contact> billingContact;

    public String getPartnerName() {
      return partnerName;
    }

    public String getPartnerType() {
      return partnerType;
    }

    public Option<Contact> getBillingContact() {
      return billingContact;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.MULTI_LINE_STYLE);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  public String getAddonLicenseId() {
    return addonLicenseId;
  }

  public Option<String> getHostLicenseId() {
    return hostLicenseId;
  }

  public String getLicenseId() {
    return licenseId;
  }

  public String getAddonKey() {
    return addonKey;
  }

  public String getAddonName() {
    return addonName;
  }

  public String getHosting() {
    return hosting;
  }

  public LocalDate getLastUpdated() {
    return lastUpdated;
  }

  public String getLicenseType() {
    return licenseType;
  }

  public LocalDate getMaintenanceStartDate() {
    return maintenanceStartDate;
  }

  public LocalDate getMaintenanceEndDate() {
    return maintenanceEndDate;
  }

  public String getStatus() {
    return status;
  }

  public String getTier() {
    return tier;
  }

  public ContactDetails getContactDetails() {
    return contactDetails;
  }

  public Option<PartnerDetails> getPartnerDetails() {
    return partnerDetails;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public String toPretty() {
    final PrettyPrinter printer = new PrettyPrinter();
    printer.addProperty("License Type", licenseType);
    printer.addProperty("Status", status);
    printer.addProperty("Maintenance End Date",
        maintenanceEndDate != null ? maintenanceEndDate.toString() : null);
    printer.addProperty("Tier", tier);
    printer.addProperty("Company", calcCompanyName());
    return printer.toString();
  }

  private String calcCompanyName() {
    String company = null;
    if (contactDetails != null) {
      company = contactDetails.company;
    }

    if (StringUtils.isBlank(company)) {
      company = partnerDetails.getOrElse(new PartnerDetails()).partnerName;
    }

    return company;
  }
}
