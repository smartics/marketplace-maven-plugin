/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.marketplace.client.api;

import static com.google.common.collect.ImmutableList.of;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Enumeration of valid license types.
 * <p>
 * If specified, restricts the query to values with the provided license type;
 * 'inactive' queries for expired licenses. Queries for 'starter' are
 * deprecated; results may not be as expected.
 * </p>
 *
 * @see <a href=
 *      "https://developer.atlassian.com/platform/marketplace/rest/#api-vendors-vendorId-reporting-licenses-get">GET
 *      /rest/2/vendors/{vendorId}/reporting/licenses</a>
 */
public enum LicenseType implements EnumWithKey {
  // ***************************** Enumeration ******************************

  ACADEMIC("academic"),

  COMMERCIAL("commercial"),

  COMMUNITY("community"),

  DEMONSTRATION("demonstration"),

  EVALUATION("evaluation"),

  OPEN_SOURCE("open_source"),

  /**
   * Queries for 'starter' are deprecated; results may not be as expected.
   */
  @Deprecated
  STARTER("starter");

  // ******************************** Fields ********************************

  // --- constants ----------------------------------------------------------

  // --- members ------------------------------------------------------------

  private final String key;

  // ***************************** Constructors *****************************

  LicenseType(final String key) {
    this.key = key;
  }

  // ******************************** Methods *******************************

  // --- init ---------------------------------------------------------------

  // --- get&set ------------------------------------------------------------

  @Override
  public String getKey() {
    return key;
  }

  // --- business -----------------------------------------------------------

  public static Iterable<LicenseType> licensedTypes() {
    return of(ACADEMIC, COMMERCIAL, COMMUNITY, OPEN_SOURCE);
  }

  public static Iterable<LicenseType> evaluationTypes() {
    return of(EVALUATION);
  }

  public static LicenseType fromString(final String licenseType) {
    for (final LicenseType current : values()) {
      if (current.key.equalsIgnoreCase(licenseType)) {
        return current;
      }
    }

    throw new IllegalArgumentException("Invalid license type: " + licenseType);
  }

  // --- object basics ------------------------------------------------------

  @Override
  public String toString() {
    return key;
  }
}
