/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.marketplace.client.impl;

import static io.atlassian.fugue.Option.some;

import de.smartics.maven.marketplace.client.model.License;

import com.atlassian.marketplace.client.model.Entity;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.RequiredLink;
import com.google.common.collect.ImmutableList;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Map;

import io.atlassian.fugue.Option;

/**
 *
 */
public class LicensesInternalModel extends InternalModel {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  /**
   * Common properties for resource collections.
   */
  public static abstract class ExtendedEntityCollection<T> implements Entity {
    private Links _links;
    private Option<Integer> count;
    @RequiredLink(rel = "self")
    private URI selfUri;

    protected ExtendedEntityCollection() {}

    protected ExtendedEntityCollection(Links links, int count) {
      this._links = links;
      this.count = some(count);
    }

    /**
     * The top-level links for the resource.
     */
    public Links getLinks() {
      return _links;
    }

    public URI getSelfUri() {
      return selfUri;
    }

    /**
     * The total number of entities in the collection, which may be more than
     * the subset in this representation.
     */
    public int getCount() {
      return count.getOrElse(0);
    }

    /**
     * A list of entities, either the full result set or a subset (page) of a
     * larger set.
     */
    public abstract Iterable<T> getItems();
  }
  public static class Licenses extends ExtendedEntityCollection<License> {
    private ImmutableList<License> licenses;

    public Iterable<License> getItems() {
      return licenses;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static Licenses licenses(final Links links,
      final ImmutableList<License> items, final int count) {
    return makeCollectionRep(Licenses.class, links, items, some(count));
  }

  // Copied from com.atlassian.marketplace.client.impl.InternalModel
  // Super hacky reflective method for creating test instances of collection
  // representations
  private static <A> A makeCollectionRep(final Class<A> repClass,
      final Links links, final ImmutableList<?> items,
      final Option<Integer> count) {
    final Map<String, Field> fields = EntityValidator.getClassFields(repClass);
    try {
      final A instance = repClass.getConstructor().newInstance();

      fields.get("_links").set(instance, links);
      for (final int c : count) {
        if (fields.get("count") != null) {
          fields.get("count").set(instance, c);
        }
      }

      final Field f = fields.get("_embedded");
      final Class<?> ec = f.getType();
      final Object e = ec.getConstructor().newInstance();
      final Field eif = ec.getDeclaredFields()[0];
      eif.setAccessible(true);
      eif.set(e, items);
      f.set(instance, e);

      return instance;
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
  }

  // --- object basics --------------------------------------------------------

}
