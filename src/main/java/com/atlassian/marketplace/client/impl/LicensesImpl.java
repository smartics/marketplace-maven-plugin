/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.marketplace.client.impl;

import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;

import de.smartics.maven.marketplace.client.api.LicenseQuery;
import de.smartics.maven.marketplace.client.api.Licenses;
import de.smartics.maven.marketplace.client.model.License;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.EnumWithKey;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.base.Function;

import org.joda.time.LocalDate;

import java.io.InputStream;

import javax.annotation.Nullable;

import io.atlassian.fugue.Option;

/**
 *
 */
public class LicensesImpl extends ApiImplBase implements Licenses {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final Function<EnumWithKey, String> toString =
      new Function<EnumWithKey, String>() {
        @Nullable
        @Override
        public String apply(@Nullable EnumWithKey enumWithKey) {
          if(enumWithKey != null) {
            return enumWithKey.getKey();
          }
          return null;
        }
      };

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  LicensesImpl(final ApiHelper apiHelper, final InternalModel.MinimalLinks root)
      throws MpacException {
    super(apiHelper, root, "licenses");
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Page<License> find(final LicenseQuery query) throws MpacException {
    final UriBuilder uri = fromApiRoot().path("reporting/licenses");

    addLicenseQueryParams(query, uri);
    return apiHelper.getMore(new PageReference<>(uri.build(), query.getBounds(),
        extendedPageReader(LicensesInternalModel.Licenses.class)));
  }

  public static void addLicenseQueryParams(final LicenseQuery q,
      final UriBuilder uriBuilder) {
    ApiHelper.addBoundsParams(q, uriBuilder);
    uriBuilder.queryParam("addon", toArray(q.getAddon(), Object.class));
    addOptionalDate(uriBuilder, "startDate", q.getStartDate());
    addOptionalDate(uriBuilder, "endDate", q.getEndDate());
    addOptionalString(uriBuilder, "text", q.getText());
    uriBuilder.queryParam("tier", toArray(q.getTier(), Object.class));
    addOptionalString(uriBuilder, "dateType", q.getDateType());
    uriBuilder.queryParam("licenseType",
        toArray(transform(q.getLicenseType(), toString), Object.class));
    uriBuilder.queryParam("partnerTypes",
        toArray(transform(q.getPartnerTypes(), toString), Object.class));
    uriBuilder.queryParam("hostingType",
        toArray(transform(q.getHosting(), toString), Object.class));
    uriBuilder.queryParam("status",
        toArray(transform(q.getStatus(), toString), Object.class));
    addOptionalDate(uriBuilder, "lastUpdated", q.getLastUpdated());
    addOptionalString(uriBuilder, "sortBy", q.getSortBy());
    addOptionalString(uriBuilder, "order", q.getOrder());
  }

  private static void addOptionalDate(final UriBuilder uri, final String name,
      final Option<LocalDate> value) {
    for (final LocalDate v : value) {
      uri.queryParam(name, v.toString());
    }
  }

  private static void addOptionalString(final UriBuilder uri, final String name,
      final Option<String> value) {
    for (final String v : value) {
      uri.queryParam(name, v);
    }
  }

  protected <T, U extends LicensesInternalModel.ExtendedEntityCollection<T>> PageReader<T> extendedPageReader(
      final Class<U> collectionRepClass) {
    return new PageReader<T>() {
      @Override
      public Page<T> readPage(PageReference<T> ref, InputStream in)
          throws MpacException {
        U rep = apiHelper.decode(in, collectionRepClass);
        return new PageImpl<T>(ref, rep.getLinks(), rep.getItems(),
            rep.getCount(), this);
      }
    };
  }

  // --- object basics --------------------------------------------------------
}
