/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.marketplace.client.impl;

import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.google.common.base.Preconditions.checkNotNull;

import de.smartics.maven.marketplace.client.api.Licenses;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonCategories;
import com.atlassian.marketplace.client.api.Addons;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.api.Assets;
import com.atlassian.marketplace.client.api.LicenseTypes;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.Products;
import com.atlassian.marketplace.client.api.Vendors;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.ModelBuilders.LinksBuilder;
import com.atlassian.marketplace.client.model.VendorBase;

import java.net.URI;

/**
 *
 */
public class ExtendedMarketplaceClient implements MarketplaceClient {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MarketplaceClient marketplaceClient;

  protected final URI baseUri;
  protected final HttpTransport httpTransport;
  protected final EntityEncoding encoding;

  private final ApiHelper apiHelper;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ExtendedMarketplaceClient(final URI baseUri,
      final MarketplaceClient marketplaceClient) {
    this(baseUri, marketplaceClient, new JsonEntityEncoding());
  }

  private ExtendedMarketplaceClient(final URI serverBaseUri,
      final MarketplaceClient marketplaceClient,
      final EntityEncoding encoding) {
    this.marketplaceClient = marketplaceClient;
    this.baseUri = ApiHelper
        .normalizeBaseUri(checkNotNull(serverBaseUri, "serverBaseUri"));
    // .resolve("rest/" + DefaultMarketplaceClient.API_VERSION + "/");
    final HttpTransport httpTransport = marketplaceClient.getHttp();
    this.httpTransport = checkNotNull(httpTransport, "httpTransport");
    this.encoding = encoding;

    this.apiHelper = new ApiHelper(this.baseUri, httpTransport, encoding);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public Licenses licenses(final VendorBase vendor) throws MpacException {
    return new LicensesImpl(apiHelper, getRoot(vendor));
  }

  private InternalModel.MinimalLinks getRoot(final VendorBase vendor)
      throws MpacException {
    final URI licensesUri = baseUri.resolve(vendor.getId().getUri());

    final LinksBuilder builder = links();
    builder.put("licenses", licensesUri);
    final Links links = builder.build();
    return InternalModel.minimalLinks(links);

//    return apiHelper.getEntity(licensesUri, LicensesInternalModel.Licenses.class);
  }

  public MarketplaceClient getMarketplaceClient() {
    return marketplaceClient;
  }

  @Override
  public boolean isReachable() {
    return marketplaceClient.isReachable();
  }

  @Override
  public Addons addons() throws MpacException {
    return marketplaceClient.addons();
  }

  @Override
  public AddonCategories addonCategories() throws MpacException {
    return marketplaceClient.addonCategories();
  }

  @Override
  public Applications applications() throws MpacException {
    return marketplaceClient.applications();
  }

  @Override
  public Assets assets() throws MpacException {
    return marketplaceClient.assets();
  }

  @Override
  public LicenseTypes licenseTypes() throws MpacException {
    return marketplaceClient.licenseTypes();
  }

  @Override
  public Products products() throws MpacException {
    return marketplaceClient.products();
  }

  @Override
  public Vendors vendors() throws MpacException {
    return marketplaceClient.vendors();
  }

  @Override
  public <T> Page<T> getMore(final PageReference<T> ref) throws MpacException {
    return marketplaceClient.getMore(ref);
  }

  @Override
  public Links getRootLinks() throws MpacException {
    return marketplaceClient.getRootLinks();
  }

  @Override
  public HttpTransport getHttp() {
    return marketplaceClient.getHttp();
  }

  @Override
  public <T> String toJson(final T entity) throws MpacException {
    return marketplaceClient.toJson(entity);
  }

  @Override
  public void close() {
    marketplaceClient.close();
  }

  // --- object basics --------------------------------------------------------

}
