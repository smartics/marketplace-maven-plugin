<?xml version='1.0'?>

<!--suppress UnresolvedMavenProperty -->
<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>de.smartics.maven.plugin</groupId>
  <artifactId>marketplace-maven-plugin</artifactId>
  <version>0.1.0-SNAPSHOT</version>
  <packaging>maven-plugin</packaging>

  <name>smartics Marketplace Maven Plugin</name>
  <description>
    Tools to manage products on the Atlassian Marketplace remotely via the REST API
    based on Maven.
  </description>
  <url>${weburl}</url>
  <inceptionYear>2019</inceptionYear>
  <organization>
    <name>smartics</name>
    <url>http://www.smartics.de/</url>
  </organization>

  <licenses>
    <license>
      <name>The Apache Software License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
      <comments>
        Copyright 2019-2025 smartics, Kronseder &amp; Reiner GmbH
      </comments>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>robert.reiner</id>
      <name>Robert Reiner</name>
      <url>https://www.smartics.de/go/robertreiner</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
    <developer>
      <id>anton.kronseder</id>
      <name>Anton Kronseder</name>
      <url>https://www.smartics.de/go/antonkronseder</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
  </developers>

  <scm>
    <connection>scm:git:https://bitbucket.org/smartics/${project.artifactId}</connection>
    <developerConnection>scm:git:https://bitbucket.org/smartics/${project.artifactId}</developerConnection>
    <url>https://bitbucket.org/smartics/${project.artifactId}</url>
    <tag>HEAD</tag>
  </scm>

  <issueManagement>
    <system>jira</system>
    <url>${info.server.issues}/MMP</url>
  </issueManagement>

  <distributionManagement>
    <repository>
      <id>${info.project.visibility}</id>
      <name>internal smartics release repository</name>
      <url>${build.server.repo.upload}/${info.project.visibility}</url>
    </repository>
    <snapshotRepository>
      <uniqueVersion>false</uniqueVersion>
      <id>${info.project.visibility}-snapshot</id>
      <name>internal smartics snapshot repository</name>
      <url>${build.server.repo.upload}/${info.project.visibility}-snapshot</url>
    </snapshotRepository>
    <site>
      <id>${info.project.visibility}-site</id>
      <name>documentation site</name>
      <url>${build.server.site.upload}/${project.groupId}/${project.artifactId}/${project.version}</url>
    </site>
  </distributionManagement>

  <repositories>
    <repository>
      <id>smartics-${info.project.visibility}</id>
      <url>${info.server.repo.group}</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>smartics-${info.project.visibility}-snapshot</id>
      <url>${info.server.repo.group}</url>
      <releases>
        <enabled>false</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </repository>
  </repositories>

  <properties>
    <twitterId>smartics</twitterId>

    <info.project.visibility>public</info.project.visibility>

    <info.server.web>https://www.smartics.eu/confluence/display</info.server.web>
    <info.server.issues>https://www.smartics.eu/jira/projects</info.server.issues>
    <info.server.repo>https://www.smartics.eu/nexus</info.server.repo>
    <info.server.scm>https://www.smartics.eu/svn/public</info.server.scm>

    <info.server.repo.prefix>${info.server.repo}/content</info.server.repo.prefix>
    <info.server.repo.group>${info.server.repo.prefix}/groups/public-group</info.server.repo.group>

    <build.server.site.upload>scp://www.smartics.eu/home/smartics/public_html/${info.project.visibility}</build.server.site.upload>
    <build.server.repo.upload>dav:${info.server.repo.prefix}/repositories</build.server.repo.upload>

    <!-- The following properties are accessible from site documentation and therefore contain no dots. -->
    <webroot>https://www.smartics.eu</webroot>
    <weburl>${webroot}/${project.groupId}/${project.artifactId}/${project.version}</weburl>
    <scm-weburl>https://www.smartics.eu/svn/public</scm-weburl> <!-- No subdir ${info.project.visibility} currently. -->
    <repo-url>https://www.smartics.eu/nexus</repo-url> <!-- HTTPs -->
    <repo-download-url>${repo-url}/service/local/artifact/maven/redirect</repo-download-url>

    <webroot-projects>http://www.smartics.de/projects</webroot-projects>
    <wiki-url>https://www.smartics.eu/confluence</wiki-url>

    <!-- Project settings -->
    <build.java.version>1.8</build.java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <!-- Plugin Versions -->
    <maven-fluido-smartics-skin_version>2.1.0</maven-fluido-smartics-skin_version>

    <version.buildmetadata-maven-plugin>1.7.0</version.buildmetadata-maven-plugin>

    <version.maven-compiler-plugin>3.1</version.maven-compiler-plugin>
    <version.maven-clean-plugin>2.6.1</version.maven-clean-plugin>
    <version.maven-dependency-plugin>2.10</version.maven-dependency-plugin>
    <version.maven-resources-plugin>2.7</version.maven-resources-plugin>
    <version.maven-deploy-plugin>2.8.2</version.maven-deploy-plugin>
    <version.maven-jar-plugin>2.6</version.maven-jar-plugin>
    <version.maven-release-plugin>2.5.2</version.maven-release-plugin>
    <version.maven-install-plugin>2.5.1</version.maven-install-plugin>
    <version.maven-scm-plugin>1.9.4</version.maven-scm-plugin>
    <version.maven-project-info-reports-plugin>2.7</version.maven-project-info-reports-plugin>
    <version.versions-maven-plugin>2.1</version.versions-maven-plugin>
    <version.maven-site-plugin>3.4</version.maven-site-plugin>
    <version.maven-license-plugin>1.9.0</version.maven-license-plugin>
    <version.maven-javadoc-plugin>2.10.3</version.maven-javadoc-plugin>

    <!--  Dependency versions -->
    <version.maven>3.2.1</version.maven>
    <version.maven-plugin-plugin>3.5.2</version.maven-plugin-plugin>
    <version.maven-plugin-annotations>3.5.2</version.maven-plugin-annotations>

    <version.marketplace-client>3.0.1</version.marketplace-client>
    <version.fugue>4.7.2</version.fugue>

    <version.smartics-test-utils>0.3.3</version.smartics-test-utils>
    <version.junit>4.12</version.junit>
  </properties>

  <!--  ... maven ....................................................... -->

  <dependencies>
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>annotations</artifactId>
      <version>3.0.1</version>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugin-tools</groupId>
      <artifactId>maven-plugin-annotations</artifactId>
      <version>${version.maven-plugin-annotations}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
      <version>${version.maven}</version>
      <exclusions>
        <exclusion>
          <groupId>com.google.guava</groupId>
          <artifactId>guava</artifactId>
        </exclusion>
      </exclusions>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-compat</artifactId>
      <version>${version.maven}</version>
    </dependency>

    <dependency>
      <groupId>com.atlassian.marketplace</groupId>
      <artifactId>marketplace-client-java</artifactId>
      <version>${version.marketplace-client}</version>
      <classifier>dependencies</classifier>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>io.atlassian.fugue</groupId>
      <artifactId>fugue</artifactId>
      <version>${version.fugue}</version>
    </dependency>

    <dependency>
      <groupId>com.ecwid</groupId>
      <artifactId>maleorang</artifactId>
      <version>3.0-0.9.6</version>
    </dependency>

    <!-- ... test ......................................................... -->

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${version.junit}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-clean-plugin</artifactId>
          <version>${version.maven-clean-plugin}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${version.maven-dependency-plugin}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${version.maven-resources-plugin}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${version.maven-deploy-plugin}</version>
          <dependencies>
            <dependency>
              <groupId>org.apache.maven.wagon</groupId>
              <artifactId>wagon-webdav-jackrabbit</artifactId>
              <version>2.12</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${version.maven-jar-plugin}</version>
          <configuration>
            <archive>
              <index>true</index>
              <manifest>
                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
              </manifest>
              <manifestEntries>
                <Implementation-SCM-Revision-Number>${build.scmRevision.id}</Implementation-SCM-Revision-Number>
                <Implementation-SCM-Revision-Date>${build.scmRevision.date}</Implementation-SCM-Revision-Date>
                <Implementation-URL>${project.url}</Implementation-URL>
                <Implementation-Date>${build.date}</Implementation-Date>
                <Implementation-Timestamp>${build.timestamp.millis}</Implementation-Timestamp>
                <Implementation-DatePattern>${build.date.pattern}</Implementation-DatePattern>
                <Implementation-Full-Version>${build.version.full}</Implementation-Full-Version>
                <Built-OS>${os.name} / ${os.arch} / ${os.version}</Built-OS>
                <Built-By>${build.user}</Built-By>
                <Maven-Version>${build.maven.version}</Maven-Version>
                <Java-Version>${java.version}</Java-Version>
                <Java-Vendor>${java.vendor}</Java-Vendor>
              </manifestEntries>
            </archive>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-install-plugin</artifactId>
          <version>${version.maven-install-plugin}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>${version.maven-release-plugin}</version>
          <configuration>
            <useReleaseProfile>false</useReleaseProfile>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-scm-plugin</artifactId>
          <version>${version.maven-scm-plugin}</version>
          <configuration>
            <connectionType>developerConnection</connectionType>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <version>${version.maven-project-info-reports-plugin}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${version.maven-site-plugin}</version>
          <dependencies>
            <dependency>
              <!-- add support for ssh/scp -->
              <groupId>org.apache.maven.wagon</groupId>
              <artifactId>wagon-ssh</artifactId>
              <version>3.2.0</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <id>attach-descriptor</id>
              <goals>
                <goal>attach-descriptor</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <locales>en</locales>
            <relativizeDecorationLinks>false</relativizeDecorationLinks>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${version.versions-maven-plugin}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>${version.maven-javadoc-plugin}</version>
          <configuration>
            <skip>true</skip>
          </configuration>
        </plugin>

        <!--
          This configuration is for synchronizing Eclipse configuration only.
            https://docs.sonatype.org/display/M2ECLIPSE/Customizable+build+lifecycle+mapping+for+m2e+extensions+developers

          No plugin involved:
            http://dev.eclipse.org/mhonarc/lists/m2e-users/msg00657.html
        <plugin>
          <groupId>org.eclipse.m2e</groupId>
          <artifactId>lifecycle-mapping</artifactId>
          <version>1.0.0</version>
          <configuration>
            <lifecycleMappingMetadata>
              <pluginExecutions>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>buildmetadata-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>provide-buildmetadata</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>com.redhat.rcm.maven.plugin</groupId>
                    <artifactId>buildmetadata-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>provide-buildmetadata</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>

                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <versionRange>[1.0,)</versionRange>
                    <goals>
                      <goal>enforce</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>

                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>copy-dependencies</goal>
                      <goal>unpack</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>

                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-toolchains-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>toolchain</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>smartics-jboss-modules-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>create-modules-archive</goal>
                      <goal>index</goal>
                      <goal>jandex</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-plugin-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>helpmojo</goal>
                      <goal>descriptor</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
              </pluginExecutions>
            </lifecycleMappingMetadata>
          </configuration>
        </plugin>          -->
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${version.maven-compiler-plugin}</version>
        <configuration>
          <source>${build.java.version}</source>
          <target>${build.java.version}</target>
        </configuration>
      </plugin>
      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>buildmetadata-maven-plugin</artifactId>
        <version>${version.buildmetadata-maven-plugin}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>provide-buildmetadata</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <buildDatePattern>dd.MM.yyyy HH:mm:ss</buildDatePattern>
          <remoteVersion>origin/master</remoteVersion>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-plugin-plugin</artifactId>
        <version>${version.maven-plugin-plugin}</version>
        <executions>
          <execution>
            <id>mojo-descriptor</id>
            <goals>
              <goal>descriptor</goal>
            </goals>
          </execution>
          <execution>
            <id>help-goal</id>
            <goals>
              <goal>helpmojo</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <!-- see http://jira.codehaus.org/browse/MNG-5346 -->
          <skipErrorNoDescriptorsFound>true</skipErrorNoDescriptorsFound>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.mycila.maven-license-plugin</groupId>
        <artifactId>maven-license-plugin</artifactId>
        <version>${version.maven-license-plugin}</version>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <strictCheck>true</strictCheck>
          <header>src/etc/license/header.txt</header>
          <headerDefinitions>
            <headerDefinition>src/etc/license/javadoc.xml</headerDefinition>
          </headerDefinitions>
          <properties>
            <year>${build.copyright.year}</year>
          </properties>
          <excludes>
            <exclude>**/.pmd</exclude>
            <exclude>**/.checkstyle</exclude>
            <exclude>**/.ruleset</exclude>
            <exclude>**/COPYRIGHT.txt</exclude>
            <exclude>**/LICENSE.txt</exclude>
            <exclude>**/LICENSE-*</exclude>
            <exclude>**/*.xcf</exclude>
            <exclude>**/javadoc.xml</exclude>
            <exclude>**/header.txt</exclude>
            <exclude>**/*.gitignore</exclude>

            <exclude>**/*.md</exclude>

            <exclude>**/*.json</exclude>

            <exclude>.idea/**</exclude>
          </excludes>
          <mapping>
            <!--  Unfortunately we use xml.vm extensions to filter xdoc pages.
                  The tag xml.vm is not recognized, so we simply override the
                  vm tag. -->
            <vm>XML_STYLE</vm>
            <jnlp>XML_STYLE</jnlp>
          </mapping>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${version.maven-project-info-reports-plugin}</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>cim</report>
              <report>dependencies</report>
              <report>dependency-convergence</report>
              <report>dependency-management</report>
              <!-- <report>distribution-management</report> -->
              <report>index</report>
              <report>issue-tracking</report>
              <report>license</report>
              <!-- <report>mailing-list</report> -->
              <report>modules</report>
              <report>plugin-management</report>
              <report>project-team</report>
              <report>scm</report>
              <report>summary</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>

      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>buildmetadata-maven-plugin</artifactId>
        <version>${version.buildmetadata-maven-plugin}</version>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-plugin-plugin</artifactId>
        <version>${version.maven-plugin-plugin}</version>
        <configuration>
          <requirements>
            <maven>3.1</maven>
          </requirements>
        </configuration>
      </plugin>
    </plugins>
  </reporting>

  <profiles>
    <profile>
      <id>release</id>

      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <properties>
        <version.maven-source-plugin>2.4</version.maven-source-plugin>
        <version.maven-gpg-plugin>1.6</version.maven-gpg-plugin>
      </properties>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>${version.maven-source-plugin}</version>
            <executions>
              <execution>
                <id>attach-sources</id>
                <goals>
                  <goal>jar-no-fork</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <artifactId>maven-deploy-plugin</artifactId>
            <version>${version.maven-deploy-plugin}</version>
            <configuration>
              <updateReleaseInfo>true</updateReleaseInfo>
            </configuration>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <version>${version.maven-gpg-plugin}</version>
            <executions>
              <execution>
                <id>sign-artifacts</id>
                <phase>verify</phase>
                <goals>
                  <goal>sign</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
